package com.playsql.dialect.model.audittrail;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.Serializable;
import java.util.Objects;

/**
 * The configuration of the audit trail feature
 */
public class AuditTrailStatus implements Serializable {

    public enum Policy { KEEP_ALL, TIME_BASED, COUNT_BASED }

    private boolean enabled;
    private Policy policy;
    private Integer duration;

    public AuditTrailStatus(boolean enabled, Policy policy, Integer duration) {
        this.enabled = enabled;
        this.policy = policy;
        this.duration = duration;
    }

    public AuditTrailStatus(){}

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return String.format("AuditTrailStatus{%s, %s, %d}",
                Objects.toString(enabled),
                Objects.toString(policy),
                duration);
    }
}
