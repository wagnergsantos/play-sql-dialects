package com.playsql.dialect.model.audittrail;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.model.DDLColumn;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * The record of a change.
 */
public class AuditTrailItem {

    /** The prefix for tables/columns we hide (From a user's subjective view, we act like we delete it, but
     * it's actually just renamed.
     */
    public static final String DELETED_PREFIX = "_DELETED_";

    private int id;
    private OperationType operation;
    private String tableName;
    private String columnName;
    private Integer rowId;
    private String author;
    private Calendar timestamp;
    private String comments;
    private DDLColumn.DataType dataType;
    private String oldValue;
    private String newValue;

    public AuditTrailItem() {
    }

    public AuditTrailItem(OperationType operation,
                          String tableName, String columnName, Integer rowId,
                          String author, Calendar timestamp, String comments,
                          DDLColumn.DataType dataType, String oldValue, String newValue) {
        this.operation = operation;
        this.tableName = tableName;
        this.columnName = columnName;
        this.rowId = rowId;
        this.author = author;
        this.timestamp = timestamp;
        this.comments = comments;
        this.dataType = dataType;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public AuditTrailItem(OperationType operation,
                          String tableName, String columnName, Integer rowId,
                          Stamp stamp,
                          DDLColumn.DataType dataType, String oldValue, String newValue) {
        this.operation = operation;
        this.tableName = tableName;
        this.columnName = columnName;
        this.rowId = rowId;
        this.author = stamp.author;
        this.timestamp = stamp.timestamp;
        this.comments = stamp.comments;
        this.dataType = dataType;
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public Integer getRowId() {
        return rowId;
    }

    public void setRowId(Integer rowId) {
        this.rowId = rowId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Calendar getTimestamp() {
        return timestamp;
    }
    public String getTimestampText(String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(timestamp.getTime());
    }

    public void setTimestamp(Calendar timestamp) {
        this.timestamp = timestamp;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public DDLColumn.DataType getDataType() {
        return dataType;
    }

    public void setDataType(DDLColumn.DataType dataType) {
        this.dataType = dataType;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public OperationType getOperation() {
        return operation;
    }

    public void setOperation(OperationType operation) {
        this.operation = operation;
    }

    @Override
    public String toString() {
        return "AuditTrailItem{" +
                "#" + id +
                ", " + operation +
                " " + tableName +
                '.' + columnName + ':' + rowId +
                ", by " + author + " on " + timestamp +
                '}';
    }
}
