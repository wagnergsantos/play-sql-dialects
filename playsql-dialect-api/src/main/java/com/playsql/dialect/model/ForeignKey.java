package com.playsql.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.Objects;

import static com.google.common.base.Objects.equal;

/**
 * Properties of a column which is a private key to another table.
 *
 * The origin column isn't detailed here.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ForeignKey implements Cloneable {

    public enum Action {
        NO_ACTION("a", "NO ACTION"),
        RESTRICT("r", "RESTRICT"),
        CASCADE("c", "CASCADE"),
        SET_NULL("n", "SET NULL"),
        SET_DEFAULT("d", "SET DEFAULT");

        private final String sqlFlag;
        private final String sqlExpression;

        Action(String sqlFlag, String sqlExpression) {
            this.sqlFlag = sqlFlag;
            this.sqlExpression = sqlExpression;
        }

        public String getSqlExpression() {
            return sqlExpression;
        }

        public static Action parse(String fkAction) {
            if (fkAction == null) return null;
            for (Action value : values())
                if (equal(value.sqlFlag, fkAction))
                    return value;
            throw new IllegalArgumentException("Foreign key action is unparseable: " + fkAction);
        }
    }

    /**
     * Whether this is a hard-wired foreign key or an emulation. An emulation means there's no FK at the
     * db level, but the UI displays it as-if.
     *
     * Basically none of the FKs are emulations for the moment, and we plan to use them later for queries.
     */
    private boolean emulation;
    // == All properties hard-wired in SQL, unless emulation = true ==
    private String targetTable;
    private Action onDelete;
    private Action onUpdate; // If null, equals onDelete
    /** The name of the constraint in SQL */
    @XmlTransient
    private String constraintName;
    /** Name of the field containing the primary key */
    private String targetPK;

    /** == All properties stored in the metadata – See ColumnComment == */
    private String displayField;
    @XmlTransient
    private String displayFieldSQL;
    private String searchField;
    /** DDL of the display field - We need it in case the format should be displayed in a certain way. */
    private DDLColumn targetColumnDDL;

    public ForeignKey() {
        // JAXB
    }

    public ForeignKey(String constraintName, String targetTable, String targetPK, String displayField, String searchField, Action onDelete) {
        this.constraintName = constraintName;
        this.targetTable = targetTable;
        this.targetPK = targetPK;
        this.displayField = displayField;
        this.searchField = searchField;
        this.onDelete = onDelete;
        this.onUpdate = null;
    }

    public static ForeignKey copy(ForeignKey fk) {
        if (fk == null) return null;
        try {
            return (ForeignKey) fk.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getTargetTable() {
        return targetTable;
    }

    public Action getOnDelete() {
        return onDelete;
    }

    public Action getOnUpdate() {
        return onUpdate;
    }

    public String getDisplayField() {
        return displayField;
    }

    public String getSearchField() {
        return searchField;
    }

    public ForeignKey setTargetTable(String targetTable) {
        this.targetTable = targetTable;
        return this;
    }

    public ForeignKey setOnDelete(Action onDelete) {
        this.onDelete = onDelete;
        return this;
    }

    public ForeignKey setOnUpdate(Action onUpdate) {
        this.onUpdate = onUpdate;
        return this;
    }

    public ForeignKey setDisplayField(String displayField) {
        this.displayField = displayField;
        return this;
    }

    public ForeignKey setSearchField(String searchField) {
        this.searchField = searchField;
        return this;
    }

    public void setConstraintName(String constraintName) {
        this.constraintName = constraintName;
    }

    public String getTargetPK() {
        return targetPK;
    }

    public void setTargetPK(String targetPK) {
        this.targetPK = targetPK;
    }

    public DDLColumn getTargetColumnDDL() {
        return targetColumnDDL;
    }

    public void setTargetColumnDDL(DDLColumn targetColumnDDL) {
        this.targetColumnDDL = targetColumnDDL;
    }

    public String getDisplayFieldSQL() {
        return displayFieldSQL;
    }

    public void setDisplayFieldSQL(String displayFieldSQL) {
        this.displayFieldSQL = displayFieldSQL;
    }

    public String getConstraintName() {
        return constraintName;
    }

    @Override
    public String toString() {
        return String.format("FK{%s -> %s.%s, display=%s, search=%s, %s/%s%s}",
            constraintName,
            targetTable,
            targetPK,
            displayField,
            searchField,
            Objects.toString(onDelete),
            Objects.toString(onUpdate),
            targetColumnDDL != null ? " ddl" : "");
    }
}
