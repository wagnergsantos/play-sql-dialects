package com.playsql.dialect.model.formulas;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Represents a cell or a range of cells.
 *
 * It's used when managing the dependencies of a formula.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public class Endpoint {
    private String tableName;
    private String columnName;
    private Long rowId;
    /**
     * The formula.  It is simpler to store it here for {@link FormulaDependency.EndpointRole#DESTINATION} endpoints.
     * It's ignored for {@link FormulaDependency.EndpointRole#ORIGIN} endpoints.
     */
    private String formula;

    public Endpoint(@Nonnull String tableName, @Nullable String columnName, @Nullable Long rowId) {
        this.tableName = tableName;
        this.columnName = columnName;
        this.rowId = rowId;
    }

    public Endpoint(@Nonnull String tableName, @Nullable String columnName, @Nullable Long rowId, @Nullable String formula) {
        this.tableName = tableName;
        this.columnName = columnName;
        this.rowId = rowId;
        this.formula = formula;
    }

    public String getTableName() {
        return tableName;
    }

    public String getColumnName() {
        return columnName;
    }

    public Long getRowId() {
        return rowId;
    }

    public String getFormula() {
        return formula;
    }

    /**
     * Two endpoints are equal if they exactly cover the same cell/cells. The formula isn't compared.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Endpoint endpoint = (Endpoint) o;

        if (columnName != null ? !columnName.equals(endpoint.columnName) : endpoint.columnName != null)
            return false;
        if (rowId != null ? !rowId.equals(endpoint.rowId) : endpoint.rowId != null) return false;
        if (!tableName.equals(endpoint.tableName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = tableName.hashCode();
        result = 31 * result + (columnName != null ? columnName.hashCode() : 0);
        result = 31 * result + (rowId != null ? rowId.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Endpoint{" +
                tableName + '.' +
                columnName + ':' +
                rowId +
                '}';
    }
}
