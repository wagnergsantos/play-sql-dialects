package com.playsql.dialect.model.formulas;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.apache.commons.lang.NotImplementedException;

import javax.annotation.Nonnull;

/**
 * Track the dependencies of formulas
 */
public class FormulaDependency {

    public enum EndpointRole {
        /** Means this endpoint is updated when the formula is executed */
        DESTINATION,
        /** Means the endpoint is read and used by the formula */
        ORIGIN
    }

    /** The cells which are read in the formula */
    private Endpoint origin;
    /** The cells which are updated by the formula. The text of the formula must be included
     * in this one. */
    private Endpoint destination;

    public FormulaDependency(@Nonnull Endpoint origin, @Nonnull Endpoint destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public Endpoint origin() {
        return origin;
    }

    public Endpoint destination() {
        return destination;
    }

    public Endpoint get(@Nonnull EndpointRole role) {
        switch (role) {
            case ORIGIN: return origin;
            case DESTINATION: return destination;
            default:
                throw new NotImplementedException(role.toString());
        }
    }
}
