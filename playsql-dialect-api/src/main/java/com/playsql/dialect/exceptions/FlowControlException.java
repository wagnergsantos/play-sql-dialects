package com.playsql.dialect.exceptions;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/**
 * Exceptions which can be used for flow control.
 * <p/>
 * It's usually controversial to do so, but the most often quoted reason against it is, it takes time to fill in
 * the stack trace, therefore you'll see it doesn't. Of course the other reason is it breaks the principle
 * of least astonishment, which is correct, which would be better if it were a checked exception, but closures
 * in Java (implemented by a Callable) don't respect covariance of exceptions, etc, etc. but exceptions are a
 * good enough solution for now.
 * */
public class FlowControlException extends RuntimeException {

    boolean silentInProd = true;

    @Override
    public synchronized Throwable fillInStackTrace() {
        if (!silentInProd || Boolean.getBoolean("playsql.dev.mode"))
            return super.fillInStackTrace();
        else
            return this;
    }

    public FlowControlException() {
    }

    public FlowControlException(String message) {
        super(message);
    }

    public FlowControlException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlowControlException(Throwable cause) {
        super(cause);
    }

    public FlowControlException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public FlowControlException setSilentInProd(boolean silent) {
        this.silentInProd = silent;
        return this;
    }
}
