package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.spi.models.Tuple.Truple;

import java.util.List;

public interface CapabilityEntities {

    /**
     * The types of entities stored in the PLAYSQL_ENTITIES table.
     * Spreadsheets are not listed because they are not stored here, although we should think about it one day,
     * it would be more sane than table comments.
     **/
    enum EntityType {
        /** Usual Queries */
        QUERY,
        /** Joint tables. It could have been a query, but let's adopt a more integrated approach. */
        JOINT_TABLE,
        /** Pivot tables. */
        PIVOT_TABLE,
        /** Can't be stored yet in the table, but it's only a matter of time */
        SPREADSHEET
    }

    /** Creates the Queries table. Only used on write connections. */
    void upgradeTask3_createEntitiesTable();

    /**
     * Saves an entity
     * @param type the type of entity
     * @param id the ID of the query if the type is ID-based
     * @param key the key. May be null for queries. It is used for search.
     * @param label the label. For information only.
     * @param lastModified the formatted date of last modification. For information only.
     * @param sql the sql. For information only.
     * @param serialized the json of the entity.
     * @param create if the entity doesn't exist yet.
     * @return the ID of the updated record, or the arbitrarily assigned ID if create == true.
     */
    int saveEntity(EntityType type, Integer id, String key, String label, String lastModified,
                   String sql, String serialized, boolean create);

    /**
     * Gets the JSON of an entity by searching for its ID
     * @param type the type, or null to return any type
     * @return the <id, json> of the entity or null
     */
    Truple<Integer, EntityType, String> getEntity(EntityType type, int id);

    /**
     * Gets the JSON of an entity by searching for its key.
     * @param type the type, or null to return any type
     * @param key the key, never null
     * @return the entity (id, type, json) or null
     */
    Truple<Integer, EntityType, String> getEntity(EntityType type, String key);

    /**
     * Gets the JSON of the entities
     * @param type the type, never null
     * @return a list of { id, label, json }
     */
    List<Truple<Integer, String, String>> getEntities(EntityType type);

    void deleteEntity(EntityType type, String key);
    void deleteEntity(EntityType type, int id);
}
