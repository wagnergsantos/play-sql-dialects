package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.exceptions.JdbcException;
import com.playsql.dialect.model.DDLColumn;
import com.playsql.dialect.model.DDLTable;
import com.playsql.dialect.model.ForeignKey;

public interface CapabilityTableManagement {
    /**
     * Builds the DDL for the table.
     *
     * @param tablename the tablename
     * @param includeDeletedColumns whether the deleted columns should be returned
     * @return the ddl
     * @throws JdbcException if table doesn't exist
     */
    DDLTable readTableDefinition(String tablename, boolean includeDeletedColumns);

    void createTable(DDLTable table);

    void renameTable(String original, String newName);

    /** Check if {@code table} exists (case insensitive) */
    void duplicateTable(String copyOf, DDLTable newTable);

    boolean hasTable(String table);

    void dropTable(DDLTable table, boolean ifExists, DDLDialect.DropBehaviour behaviour);

    void updateTableMetadata(DDLTable result);

    void addColumn(DDLTable table, DDLColumn column, DDLColumn beforeColumn);

    void dropColumn(DDLTable table, DDLColumn column, DDLDialect.DropBehaviour behaviour);

    void renameColumn(DDLTable table, DDLColumn originalColumn, DDLColumn newColumn);

    void changeColumnType(DDLTable definition, DDLColumn originalColumn, DDLColumn newColumn);

    void moveColumn(DDLTable definition, String columnname, String before);

    void changeColumnLabelFormulaWidthFKAndRenderer(DDLTable definition, DDLColumn originalColumn, DDLColumn column);

    void changeColumnFormats(DDLTable definition, DDLColumn originalColumn, DDLColumn column);

    void fkAdd(DDLTable table, DDLColumn column, ForeignKey fk, String FK_RENDERER_KEY);

    void fkRemove(DDLTable table, DDLColumn column);

    /**
     * Return true if all values match, false otherwise.
     */
    boolean testWhetherValuesMatch(String sourceTableName, DDLColumn sourceColumn, String targetTableName, DDLColumn targetColumn);

    /**
     * Insert all values of originColumn into the target table. If insertInDisplayField, it inserts it in the
     * displayColumn, otherwise it inserts it in the ID column (requires the origin column to be integer).
     */
    void fkInsertMissingValues(String originTable, String originColumn, String targetTable, String idColumn, String displayColumn, boolean insertInDisplayField, boolean addInfoInDisplayColumn);

    void fkConvert(DDLTable originTable, DDLColumn originColumn, String key, String targetTable, String targetColumn, ForeignKey fk);
}
