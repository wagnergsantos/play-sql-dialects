package com.playsql.dialect.api;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.playsql.dialect.exceptions.CircularFormulaException;
import com.playsql.dialect.model.formulas.Endpoint;
import com.playsql.dialect.model.formulas.FormulaDependency;

import java.util.List;

public interface CapabilityFormulas {

    void upgradeTask2_createFormulaDependenciesTable();

    void upgradeTask2_upgradeExistingTables();

    /**
     * Save a single dependency of a formula. The dependency can be on a single cell,
     * on all cells of a column, on all cells of a row, or on a full table.
     */
    void saveFormulaDependency(FormulaDependency item);

    /**
     * Remove all dependencies of a cell. Use this when the user edits the cell and removes/changes
     * the formula.
     */
    void removeFormulaDependencies(Endpoint endpoint, FormulaDependency.EndpointRole role);

    /**
     * Lists the dependencies of a cell. Use it to know which cells must be updated after
     * a value was changed.
     * @param endpoint the updated cell
     * @param role always {@link FormulaDependency.EndpointRole#ORIGIN}
     * @param cascade true if you need to all dependencies recursively and in the right order, false if you only
     *                need the direct dependencies
     * @throws CircularFormulaException if the formulas are
     */
    List<Endpoint> listFormulaDependencies(Endpoint endpoint, FormulaDependency.EndpointRole role,
                                           boolean cascade, boolean includeSelf)
        throws CircularFormulaException;

    void executeFormula(Endpoint endpoint, String sql);

    /**
     * This method is quite custom: It updates all endpoints, plus it renames everywhere where the column is referenced.
     */
    void renameColumnInFormulaDependencyTable(String localTableName, String oldColumnName, String newColumnName, List<Endpoint> endpointsToUpdate);
}
