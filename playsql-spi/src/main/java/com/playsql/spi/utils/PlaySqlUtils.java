package com.playsql.spi.utils;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang.StringUtils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaySqlUtils
{
    private final static Gson gson;
    static {
        if (Boolean.getBoolean("playsql.dev-mode")) {
            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").setPrettyPrinting().create();
        } else {
            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssX").create();
        }
    }

    public static Gson gson() {
        return gson;
    }


    public static String asString(Throwable e)
    {
        StringWriter stackTraceAsString = new StringWriter();
        PrintWriter pw = new PrintWriter(stackTraceAsString);
        e.printStackTrace(pw);
        return stackTraceAsString.toString();
    }

    public static Integer parseInt(String i) {
        if (StringUtils.isBlank(i)) return null;
        return Integer.parseInt(i);
    }

    public static Long parseLong(String i) {
        if (StringUtils.isBlank(i)) return null;
        return Long.parseLong(i);
    }

    public static String buildOrderBy(List<OrderByPart> clause) {
        if (clause == null || clause.isEmpty()) return null;
        StringBuilder sb = new StringBuilder();
        for (OrderByPart part : clause) {
            if (sb.length() != 0) {
                sb.append(", ");
            }
            if (part.caseSensitive) {
                sb.append('"').append(part.field).append('"');
            } else {
                sb.append(part.field);
            }
            if (part.way == OrderByPart.Way.ASC) sb.append(" ASC");
            if (part.way == OrderByPart.Way.DESC) sb.append(" DESC");
            if (part.nulls == OrderByPart.Nulls.FIRST) sb.append(" NULLS FIRST");
            if (part.nulls == OrderByPart.Nulls.LAST) sb.append(" NULLS LAST");
        }
        return sb.toString();
    }

    public static <T> T firstOf(List<T> list) {
        if (list != null)
            if (list.size() > 0)
                return list.get(0);
        return null;
    }

    public static <T> T firstNonNull(T... t) {
        if (t != null)
            for (T t1 : t)
                if (t1 != null)
                    return t1;
        return null;
    }
    public static <T> T firstNonBlank(T... t) {
        if (t != null)
            for (T t1 : t)
                if (t1 != null && StringUtils.isNotBlank(Objects.toString(t1)))
                    return t1;
        return null;
    }

    public static class OrderByPart {
        public enum Way { ASC, DESC };
        public enum Nulls { FIRST, LAST };
        public final String field;
        public final Way way;
        public final Nulls nulls;
        public final boolean caseSensitive;

        public OrderByPart(String field, Way way, Nulls nulls, boolean caseSensitive) {
            this.field = field;
            this.way = way;
            this.nulls = nulls;
            this.caseSensitive = caseSensitive;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            OrderByPart that = (OrderByPart) o;

            if (!field.equals(that.field)) return false;
            if (nulls != that.nulls) return false;
            if (way != that.way) return false;

            return true;
        }

        @Override
        public int hashCode() {
            return field.hashCode();
        }

        @Override
        public String toString() {
            return "OrderByPart{" +
                    "" + field +
                    " " + way +
                    " " + nulls +
                    (caseSensitive ? " case-sensitive" : "") +
                    '}';
        }
    }
    private static Pattern orderByExpression = Pattern.compile("[\\s]*(" +
        // Either a field name between double quotes
        "\"[^\"]*\"" +
        "|" +
        // Either a field name without quotes, without space or comma
        "[^ ,]*" +
        ")" +
        // Followed by ASC/DESC/NULLS/FIRST/LAST and a comma
        "[\\s]*(ASC|DESC)?" +
        "[\\s]*(NULLS[\\s]+(FIRST|LAST))?" +
        "[\\s]*,"
                /*// Potentially followed by ASC/DESC
                "(|[Aa][Ss][Cc]|[Dd][Ee][Ss][Cc])" +
                // Potentially finished with NULLS FIRST/NULLS LAST
                "()"*/,
            Pattern.CASE_INSENSITIVE);

    /**
     * Parses the Order By clause, if simple enough
     * @param orderBy the clause
     * @return the parsed clause, or null if parsing wasn't possible
     */
    public static List<OrderByPart> parseOrderBy(String orderBy) {
        if (orderBy.trim().isEmpty()) return Lists.newArrayList();
        Matcher matcher = orderByExpression.matcher(orderBy + ",");
        List<OrderByPart> parts = Lists.newArrayList();
        int length = -1;
        while (matcher.find()) {
            String field = matcher.group(1);
            String order = matcher.group(2);
            String nulls = matcher.group(4);
            boolean caseSensitive = false;
            if (field.startsWith("\"") && field.endsWith("\"")) {
                field = field.substring(1, field.length() - 1);
                caseSensitive = true;
            }
            parts.add(new OrderByPart(field,
                    order == null ? null : order.equalsIgnoreCase("ASC") ? OrderByPart.Way.ASC : OrderByPart.Way.DESC,
                    nulls == null ? null : nulls.equalsIgnoreCase("FIRST") ? OrderByPart.Nulls.FIRST : OrderByPart.Nulls.LAST,
                    caseSensitive
            ));
            length += matcher.group(0).length();
        }
        // Can't parse it?
        if (length != orderBy.length()) return null;
        return parts;
    }


}
