package com.playsql.spi;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Map;

/**
 * This provides a template engine. Mostly used to render queries.
 * Velocity is the recommended implementation, though you're free to provide yours.
 */
public interface TemplateEngine {
    String DEFAULT_ENGINE = "velocity";
    String render(String engine, String template, Map<String, Object> parameters);
    String renderTemplate(String engine, String path, Map<String, Object> parameters);
}