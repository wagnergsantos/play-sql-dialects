
# How to contribute #

* Your commit messages must start with an issue key, such as "PLAYSQL-119 Description of the changes",
* Please update the modified files with the list of your changes (as described in the license),
* Please add your name or the one of your company to the list of contributors for attribution,
* Please submit the change as a pull request in BitBucket.

Thank you!

# Attribution notices #

## For the current project

Includes the modules named "playsql-dialect-parent", "playsql-dialect-api", "playsql-dialects-plugin" and "playsql-spi".

Author: Adrien Ragot

Contributors:

* Be the first!

## For dependencies

* (Public Domain) AOP alliance (aopalliance:aopalliance:1.0 - http://aopalliance.sourceforge.net)
* (The Apache Software License, Version 2.0) FindBugs-jsr305 (com.google.code.findbugs:jsr305:1.3.9 - http://findbugs.sourceforge.net/)
* (The Apache Software License, Version 2.0) Gson (com.google.code.gson:gson:1.6 - http://code.google.com/p/google-gson/)
* (The Apache Software License, Version 2.0) Guava: Google Core Libraries for Java (com.google.guava:guava:10.0.1 - http://code.google.com/p/guava-libraries/guava)
* (The Apache Software License, Version 2.0) Play SQL - SPI (com.playsql:playsql-spi:1.0-SNAPSHOT - no url defined)
* (The Apache Software License, Version 2.0) Commons Lang (commons-lang:commons-lang:2.4 - http://commons.apache.org/lang/)
* (The Apache Software License, Version 2.0) Commons Logging (commons-logging:commons-logging:1.1.1 - http://commons.apache.org/logging)
* (HSQLDB License, a BSD open source license) HyperSQL Database (org.hsqldb:hsqldb:2.2.9 - http://hsqldb.org)
* (The PostgreSQL License) PostgreSQL JDBC Driver (org.postgresql:postgresql:9.4-1200-jdbc4 - http://jdbc.postgresql.org)
* (The Apache Software License, Version 2.0) Spring Framework: Beans (org.springframework:spring-beans:2.5.6.SEC02 - http://www.springframework.org)
* (The Apache Software License, Version 2.0) Spring Framework: Context (org.springframework:spring-context:2.5.6.SEC02 - http://www.springframework.org)
* (The Apache Software License, Version 2.0) Spring Framework: Core (org.springframework:spring-core:2.5.6.SEC02 - http://www.springframework.org)
* (The Apache Software License, Version 2.0) Spring Framework: JDBC (org.springframework:spring-jdbc:2.5.6.SEC02 - http://www.springframework.org)
* (The Apache Software License, Version 2.0) Spring Framework: Transaction (org.springframework:spring-tx:2.5.6.SEC02 - http://www.springframework.org)
