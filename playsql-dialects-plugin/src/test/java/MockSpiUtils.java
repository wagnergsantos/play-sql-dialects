/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.playsql.spi.SpiUtils;
import org.apache.commons.lang.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class MockSpiUtils implements SpiUtils {

    private final String defaultUserName;

    public MockSpiUtils(String defaultUserName) {
        this.defaultUserName = defaultUserName;
    }

    @Override
    public <T> Class<T> loadClass(String className, Class<?> callingClass) throws ClassNotFoundException {
        return (Class<T>) Class.forName(className);
    }

    @Override
    public String htmlEncode(String text) {
        if (StringUtils.isBlank(text))
            return "";

        StringBuilder result = new StringBuilder();

        // looping is faster than multiple calls to replaceAll.
        for (int i = 0; i < text.length(); i++)
        {
            char c = text.charAt(i);
            // encode standard ASCII characters into HTML entities where needed
            switch (c)
            {
                case '\'':
                    result.append("&#39;"); // was &apos; CONF-6494
                    break;
                case '"':
                    result.append("&quot;");
                    break;
                case '<':
                    result.append("&lt;");
                    break;
                case '>':
                    result.append("&gt;");
                    break;
                case '&':
                    result.append("&amp;");
                    break;
                default:
                    result.append(c);
            }
        }

        return result.toString();
    }

    @Override
    public String urlEncode(String part) {
        try {
            return URLEncoder.encode(part, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String getConfluenceHome() {
        return "target";
    }

    @Override
    public String getI18n(String message, Object... arguments) {
        return message + StringUtils.join(arguments, ", ");
    }

    @Override
    public String getI18nHtml(String message, Object... arguments) {
        return getI18n(message, arguments);
    }

    @Override
    public String getGroup(String groupname) {
        return groupname;
    }

    @Override
    public SpiUser getUserByUsername(String username) {
        return new SpiUser(username, username, username, null);
    }

    @Override
    public SpiUser getUserByKey(String username) {
        return new SpiUser(username, username, username, null);
    }

    @Override
    public SpiUser getLoggedInUser() {
        return new SpiUser(defaultUserName, defaultUserName, defaultUserName, null);
    }

    @Override
    public String getLoggedInUserName() {
        return defaultUserName;
    }
}
