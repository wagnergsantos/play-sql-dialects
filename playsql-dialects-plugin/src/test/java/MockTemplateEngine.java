/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */
import com.playsql.spi.TemplateEngine;
import org.apache.commons.lang.NotImplementedException;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.StringWriter;
import java.util.Map;

public class MockTemplateEngine implements TemplateEngine {
    @Override
    public String render(String engine, String template, Map<String, Object> parameters) {
        if (!TemplateEngine.DEFAULT_ENGINE.equals(engine)) {
            throw new NotImplementedException("Template engine not implemented: " + engine);
        }
        StringWriter tempWriter = new StringWriter(template.length());
        try {
            new VelocityEngine().evaluate(new VelocityContext(parameters),
                    tempWriter,
                    "MockTemplateEngine.velocity",
                    template);

        } catch (Exception e) {
            throw new RuntimeException("Error while rendering the template: " + template, e);
        }
        return tempWriter.toString();
    }

    @Override
    public String renderTemplate(String engine, String path, Map<String, Object> parameters) {
        throw new NotImplementedException(this.getClass().getName() + ".renderTemplate");
    }
}
