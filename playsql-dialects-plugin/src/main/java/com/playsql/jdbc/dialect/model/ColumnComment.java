package com.playsql.jdbc.dialect.model;

/*
 * #%L
 * Play SQL Dialects
 * %%
 * Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Map;

/**
 * This class helps jsonize the metadata about a column, to store it in
 * the table comments.
 *
 * <p/>
 * Since dialects are free to store metadata in the table comments or in a
 * system table, this class must stay together with <i>implementations</i> of
 * dialects.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ColumnComment {
    private String label;
    private String renderer;
    private String viewRenderer;
    private String formula;
    private Integer width;
    private Map<String, String> formats;
    /** The field to diplay when the column is a foreign key. This is in SQL. Please include
     * the quotes around field names. */
    private String fkDisplayField;
    /** The field to search from keyboard when the column is a foreign key. This is in SQL. Please include
     * the quotes around field names. */
    private String fkSearchField;

    public ColumnComment(String label, String renderer, String viewRenderer, String formula, Integer width, Map<String, String> formats, String fkDisplayField, String fkSearchField) {
        this.label = label;
        this.formula = formula;
        this.formats = formats;
        this.width = width;
        this.renderer = renderer;
        this.viewRenderer = viewRenderer;
        this.fkDisplayField = fkDisplayField;
        this.fkSearchField = fkSearchField;
    }
    public ColumnComment() {
    }

    public String label() {
        return label;
    }

    public void label(String label) {
        this.label = label;
    }

    public String renderer() {
        return renderer;
    }

    public void renderer(String renderer) {
        this.renderer = renderer;
    }

    public String viewRenderer() {
        return viewRenderer;
    }

    public void viewRenderer(String viewRenderer) {
        this.viewRenderer = viewRenderer;
    }

    public Map<String, String> formats() {
        return formats;
    }

    public void formats(Map<String, String> formats) {
        this.formats = formats;
    }

    public String formula() {
        return formula;
    }

    public void formula(String formula) {
        this.formula = formula;
    }

    public Integer width() {
        return width;
    }

    public void width(Integer width) {
        this.width = width;
    }

    public String fkDisplayField() {
        return fkDisplayField;
    }

    public void fkDisplayField(String fkDisplayField) {
        this.fkDisplayField = fkDisplayField;
    }

    public String fkSearchField() {
        return fkSearchField;
    }

    public void fkSearchField(String fkSearchField) {
        this.fkSearchField = fkSearchField;
    }
}
