/*
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
*/

package com.playsql.jdbc.dialect.templates;

import java.util.List;

public class TemplatesForPostgres extends TemplatesForGeneric {


    @Override
    public String listTablesWithoutSchema(boolean startingWith, 
                                          boolean withIntegerColumn) {
        StringBuilder sb = new StringBuilder()
            .append("\n      select TABLE_NAME")
            .append("\n      from INFORMATION_SCHEMA.TABLES t")
            .append("\n      where UPPER(TABLE_SCHEMA)=UPPER(CURRENT_SCHEMA())");
        if (startingWith) {
        sb.append("\n\n        and UPPER(TABLE_NAME) LIKE (?)");
        }
        if (withIntegerColumn) {
        sb.append("\n\n\n        and exists(")
            .append("\n            select * from information_schema.columns c")
            .append("\n            where c.table_catalog = t.table_catalog")
            .append("\n              and c.table_schema = t.table_schema")
            .append("\n              and c.table_name = t.table_name")
            .append("\n              and data_type in ('integer', 'bigint', 'smallint', 'numeric')")
            .append("\n        )");
        }
        return sb.toString();
    }


    @Override
    public String listTablesWithSchema(boolean startingWith, 
                                       boolean withIntegerColumn) {
        StringBuilder sb = new StringBuilder()
            .append("\n      select TABLE_NAME")
            .append("\n      from INFORMATION_SCHEMA.TABLES t")
            .append("\n      where UPPER(TABLE_SCHEMA)=UPPER(?)");
        if (startingWith) {
        sb.append("\n\n      and UPPER(TABLE_NAME) LIKE (?)");
        }
        if (withIntegerColumn) {
        sb.append("\n\n\n        and exists(")
            .append("\n            select * from information_schema.columns c")
            .append("\n            where c.table_catalog = t.table_catalog")
            .append("\n              and c.table_schema = t.table_schema")
            .append("\n              and c.table_name = t.table_name")
            .append("\n              and data_type in ('integer', 'bigint', 'smallint', 'numeric')")
            .append("\n        )");
        }
        return sb.toString();
    }


    @Override
    public String listTablesAndComments() {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT")
            .append("\n      CAST( CASE c.relkind")
            .append("\n        WHEN 'r' THEN 'table'")
            .append("\n        WHEN 'v' THEN 'view'")
            .append("\n        WHEN 'f' THEN 'foreign table'")
            .append("\n        END AS pg_catalog.text) as objtype,")
            .append("\n      c.oid as objectid,")
            .append("\n      c.tableoid as tableoid,")
            .append("\n      n.nspname as namespace,")
            .append("\n      CAST(c.relname AS pg_catalog.text) as tablename,")
            .append("\n      d.description com")
            .append("\n\n      FROM pg_catalog.pg_class c")
            .append("\n      LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace")
            .append("\n      LEFT JOIN pg_catalog.pg_description d ON (c.oid = d.objoid AND c.tableoid = d.classoid AND d.objsubid = 0)")
            .append("\n\n      WHERE c.relkind IN ('r', 'v', 'f')")
            .append("\n      AND n.nspname <> 'pg_catalog'")
            .append("\n      AND n.nspname <> 'information_schema'")
            .append("\n      AND pg_catalog.pg_table_is_visible(c.oid)")
            .append("\n      AND n.nspname = CURRENT_SCHEMA()");
        return sb.toString();
    }


    @Override
    public String currentUserAndSchema() {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT CURRENT_USER, CURRENT_SCHEMA");
        return sb.toString();
    }


    @Override
    public String hasSchema() {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT COUNT(*) FROM information_schema.schemata")
            .append("\n      WHERE schema_name = ?");
        return sb.toString();
    }


    @Override
    public String getCommentOnTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT table_name, obj_description(('\"' || table_name || '\"')::regclass, 'pg_class') COM")
            .append("\n      FROM information_schema.tables")
            .append("\n      WHERE table_schema=CURRENT_SCHEMA()")
            .append("\n        AND table_name = ?");
        return sb.toString();
    }


    @Override
    public String getCommentOnColumn() {
        StringBuilder sb = new StringBuilder()
            .append("\n\n      SELECT")
            .append("\n      --    a.attname as name,")
            .append("\n      pg_description.description as COMMENT")
            .append("\n      FROM pg_attribute a")
            .append("\n      LEFT JOIN pg_description ON a.attnum = pg_description.objsubid")
            .append("\n      WHERE a.attrelid = ('\"' || ? || '\"')::regclass")
            .append("\n      AND pg_table_is_visible(a.attrelid)")
            .append("\n      AND a.attname = ?");
        return sb.toString();
    }


    @Override
    public String listColumns() {
        StringBuilder sb = new StringBuilder()
            .append("\n      select")
            .append("\n        T.COLUMN_NAME,")
            .append("\n        T.DATA_TYPE,")
            .append("\n        col_description(('\"' || T.TABLE_NAME || '\"')::regclass, T.ORDINAL_POSITION) COM,")
            .append("\n        T.CHARACTER_MAXIMUM_LENGTH")
            .append("\n      from INFORMATION_SCHEMA.COLUMNS T")
            .append("\n      where T.TABLE_NAME = ?")
            .append("\n      and T.TABLE_SCHEMA = CURRENT_SCHEMA()")
            .append("\n      order by T.ORDINAL_POSITION");
        return sb.toString();
    }


    @Override
    public String dropTable(Object dropBehaviour, 
                            boolean ifExists, 
                            Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n\n      DROP TABLE ");
        if (ifExists) {
        sb.append(" IF EXISTS ");
        }
        sb.append("\n      \"");
        sb.append(table);
        sb.append("\"");
        sb.append(dropBehaviour);
        return sb.toString();
    }


    @Override
    public String count101(String tableName, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT COUNT(*) FROM (")
            .append("\n      SELECT 1 FROM ");
        sb.append(tableName);
        if (whereClause != null) {
        sb.append("\n\n            WHERE ");
        sb.append(whereClause);
        }
        sb.append("\n\n      LIMIT 101")
            .append("\n      ) SUBTABLE");
        return sb.toString();
    }

    public String alterColumn(String column, 
                              String newType, 
                              String table, 
                              String using) {
        StringBuilder sb = new StringBuilder()
            .append("\n      ALTER TABLE ");
        sb.append(table);
        sb.append("\n      ALTER COLUMN ");
        sb.append(column);
        sb.append(" TYPE ");
        sb.append(newType);
        if (using != null) {
        sb.append("\n USING ");
        sb.append(using);
        }
        return sb.toString();
    }


    @Override
    public String addColumn(String beforeColumn, 
                            String columnDefinition, 
                            String table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        ALTER TABLE ");
        sb.append(table);
        sb.append("\n        ADD COLUMN ");
        sb.append(columnDefinition);
        return sb.toString();
    }


    @Override
    public String readData(String fieldlist, 
                           Long limit, 
                           Long offset, 
                           String order, 
                           String tablename, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ");
        sb.append(fieldlist);
        sb.append("\n      FROM ");
        sb.append(tablename);
        sb.append(" TARGET_TABLE");
        if (whereClause != null) {
        sb.append("\n WHERE ");
        sb.append(whereClause);
        }
        if (order != null) {
        sb.append("\n ORDER BY ");
        sb.append(order);
        }
        if (limit != null) {
        sb.append("\n LIMIT ");
        sb.append(limit);
        }
        if (offset != null) {
        sb.append("\n OFFSET ");
        sb.append(offset);
        }
        return sb.toString();
    }


    @Override
    public String queryTotals(List<String> fieldlist, 
                              String subquery, 
                              String tablename) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ");
        for (int i = 0; i < fieldlist.size(); i++) {
            Object fieldDef = fieldlist.get(i);
        if (i != 0) {
        sb.append(" , ");
        }
        sb.append(fieldDef);
        }
        sb.append("\n      FROM ");
        if (subquery != null) {
        sb.append(" ( ");
        sb.append(subquery);
        sb.append("\n        ) INTERNAL_RESULTS");
        } else {
        sb.append(tablename);
        }
        sb.append("\n\n      LIMIT 1");
        return sb.toString();
    }


    @Override
    public String getLastGeneratedId() {
        StringBuilder sb = new StringBuilder()
            .append("\n      select LASTVAL()");
        return sb.toString();
    }

    public String monitoringActivity(Long pid) {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT pid, waiting, state, query, query_start, backend_start, xact_start, state_change,")
            .append("\n                   application_name, client_addr, client_hostname, client_port, datname, usename, usesysid,")
            .append("\n                   CASE state WHEN 'active' THEN 1")
            .append("\n                              WHEN 'idle in transaction' THEN 2")
            .append("\n                              WHEN 'idle' THEN 3")
            .append("\n                              ELSE 0")
            .append("\n                   END orderby,")
            .append("\n                   NOW() sysdate")
            .append("\n            FROM pg_stat_activity")
            .append("\n            WHERE query_start < (current_timestamp - interval '1 second')");
        if (pid != null) {
        sb.append("\n AND pid = ");
        sb.append(pid);
        }
        sb.append("\n            ORDER BY orderby ASC, query_start DESC");
        return sb.toString();
    }

    public String monitoringActivityKill(long pid) {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT pg_catalog.pg_terminate_backend(");
        sb.append(pid);
        sb.append(")");
        return sb.toString();
    }

    public String monitoringLocks() {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT relation::REGCLASS, locktype, database, relation, page, tuple, virtualxid, transactionid,")
            .append("\n                classid, objid, objsubid, virtualtransaction, pid, mode, granted, fastpath")
            .append("\n        FROM pg_locks");
        return sb.toString();
    }

    public String monitoringCursors() {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT name, statement, is_holdable, is_binary, is_scrollable, creation_time")
            .append("\n        FROM pg_cursors");
        return sb.toString();
    }

    public String monitoringBlocking() {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT bl.pid                 AS blocked_pid,")
            .append("\n            a.usename              AS blocked_user,")
            .append("\n            ka.query               AS blocking_statement,")
            .append("\n            now() - ka.query_start AS blocking_duration,")
            .append("\n            kl.pid                 AS blocking_pid,")
            .append("\n            ka.usename             AS blocking_user,")
            .append("\n            a.query                AS blocked_statement,")
            .append("\n            now() - a.query_start  AS blocked_duration")
            .append("\n        FROM  pg_catalog.pg_locks         bl")
            .append("\n        JOIN pg_catalog.pg_stat_activity a  ON a.pid = bl.pid")
            .append("\n        JOIN pg_catalog.pg_locks         kl ON kl.transactionid = bl.transactionid AND kl.pid != bl.pid")
            .append("\n        JOIN pg_catalog.pg_stat_activity ka ON ka.pid = kl.pid")
            .append("\n        WHERE NOT bl.granted");
        return sb.toString();
    }


}
