/*
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
*/

package com.playsql.jdbc.dialect.templates;

import java.util.List;

public class TemplatesForMySQL extends TemplatesForGeneric {


    @Override
    public String hasSchema() {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT COUNT(*)")
            .append("\n    FROM information_schema.schemata")
            .append("\n    WHERE schema_name = ?");
        return sb.toString();
    }


    @Override
    public String hasTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n    select COUNT(*)")
            .append("\n    from  INFORMATION_SCHEMA.TABLES")
            .append("\n    where LOWER(TABLE_NAME) = LOWER(?)")
            .append("\n    and TABLE_SCHEMA = SCHEMA()");
        return sb.toString();
    }


    @Override
    public String getCommentOnTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT")
            .append("\n        table_name TABLENAME,")
            .append("\n        table_comment COM")
            .append("\n    FROM INFORMATION_SCHEMA.TABLES")
            .append("\n    WHERE table_schema = SCHEMA()")
            .append("\n      AND table_name = ?");
        return sb.toString();
    }


    @Override
    public String setDefaultSchemas(Object schemaName) {
        StringBuilder sb = new StringBuilder()
            .append("\n    USE `");
        sb.append(schemaName);
        sb.append("`");
        return sb.toString();
    }


    @Override
    public String currentUserAndSchema() {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT user(), schema()");
        return sb.toString();
    }

    public String listTablesWithoutSchema(boolean startingWith) {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT table_name")
            .append("\n    FROM information_schema.tables t")
            .append("\n    WHERE upper(table_schema) = upper(SCHEMA())");
        if (startingWith) {
        sb.append("\n\n        AND upper(table_name) LIKE ?");
        }
        return sb.toString();
    }


    @Override
    public String count101(String tableName, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT COUNT(*) FROM (")
            .append("\n    SELECT 1 FROM ");
        sb.append(tableName);
        if (whereClause != null) {
        sb.append("\n\n        WHERE ");
        sb.append(whereClause);
        }
        sb.append("\n\n    LIMIT 101")
            .append("\n    ) SUBTABLE");
        return sb.toString();
    }


    @Override
    public String readData(String fieldlist, 
                           Long limit, 
                           Long offset, 
                           String order, 
                           String tablename, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n    SELECT ");
        sb.append(fieldlist);
        sb.append("\n    FROM ");
        sb.append(tablename);
        sb.append(" TARGET_TABLE");
        if (whereClause != null) {
        sb.append("\n WHERE ");
        sb.append(whereClause);
        }
        if (order != null) {
        sb.append("\n ORDER BY ");
        sb.append(order);
        }
        if (limit != null) {
        sb.append("\n LIMIT ");
        sb.append(limit);
        }
        if (offset != null) {
        sb.append("\n OFFSET ");
        sb.append(offset);
        }
        return sb.toString();
    }


}
