/*
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
*/

package com.playsql.jdbc.dialect.templates;

import java.util.List;

public class TemplatesForGeneric {

    public String currentSchema() {
        StringBuilder sb = new StringBuilder().append("select CURRENT_SCHEMA from (VALUES(1))");
        return sb.toString();
    }

    public String hasSchema() {
        StringBuilder sb = new StringBuilder().append("SELECT COUNT(*) FROM dba_users WHERE username = ?");
        return sb.toString();
    }

    public String createSchema(Object schemaName) {
        StringBuilder sb = new StringBuilder().append("CREATE SCHEMA \"");
        sb.append(schemaName);
        sb.append("\"");
        return sb.toString();
    }

    public String dropSchema(Object schemaName) {
        StringBuilder sb = new StringBuilder().append("DROP SCHEMA \"");
        sb.append(schemaName);
        sb.append("\" CASCADE");
        return sb.toString();
    }

    public String listSchemas(String name) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT n.nspname schema_name,")
            .append("\n          pg_catalog.pg_get_userbyid(n.nspowner) AS owner,")
            .append("\n          pg_catalog.array_to_string(n.nspacl, E'\\n') AS privileges,")
            .append("\n          pg_catalog.obj_description(n.oid, 'pg_namespace') AS desc")
            .append("\n        FROM pg_catalog.pg_namespace n")
            .append("\n        WHERE LOWER(n.nspname) NOT IN ('information_schema', 'public')")
            .append("\n        and LOWER(n.nspname) NOT LIKE 'pg_%'");
        if (name != null) {
        sb.append("\n and n.nspname = ? ");
        }
        sb.append("\n        ORDER BY 1");
        return sb.toString();
    }

    public String updateSchemaComment(Object comment, 
                                      Object schema) {
        StringBuilder sb = new StringBuilder().append("COMMENT ON SCHEMA ");
        sb.append(schema);
        sb.append(" IS ");
        sb.append(comment);
        return sb.toString();
    }

    public String getDefaultSchemas() {
        StringBuilder sb = new StringBuilder().append("SHOW search_path");
        return sb.toString();
    }

    public String setDefaultSchemas(Object schemaName) {
        StringBuilder sb = new StringBuilder().append("SET search_path TO \"");
        sb.append(schemaName);
        sb.append("\"");
        return sb.toString();
    }

    public String listTablesWithoutSchema(boolean startingWith, 
                                          boolean withIntegerColumn) {
        StringBuilder sb = new StringBuilder();
        return sb.toString();
    }

    public String listTablesWithSchema(boolean startingWith, 
                                       boolean withIntegerColumn) {
        StringBuilder sb = new StringBuilder()
            .append("\n      select    T.TABLE_NAME -- , C.COMMENT COM")
            .append("\n      from      INFORMATION_SCHEMA.TABLES T")
            .append("\n\n      left join INFORMATION_SCHEMA.SYSTEM_COMMENTS C")
            .append("\n                on T.TABLE_CATALOG = C.OBJECT_CATALOG")
            .append("\n                and T.TABLE_SCHEMA = C.OBJECT_SCHEMA")
            .append("\n                and T.TABLE_NAME = C.OBJECT_NAME")
            .append("\n                and C.OBJECT_TYPE='TABLE'")
            .append("\n\n      where     UPPER(TABLE_SCHEMA)=UPPER(?)");
        if (startingWith) {
        sb.append("\n\n                    and UPPER(TABLE_NAME) LIKE (?)");
        }
        return sb.toString();
    }

    public String listTablesAndComments() {
        StringBuilder sb = new StringBuilder()
            .append("\n      select T.TABLE_NAME tablename, C.COMMENT com")
            .append("\n      from INFORMATION_SCHEMA.TABLES T")
            .append("\n      left join INFORMATION_SCHEMA.SYSTEM_COMMENTS C")
            .append("\n        on T.TABLE_CATALOG = C.OBJECT_CATALOG")
            .append("\n        and T.TABLE_SCHEMA = C.OBJECT_SCHEMA")
            .append("\n        and T.TABLE_NAME = C.OBJECT_NAME")
            .append("\n        and C.OBJECT_TYPE='TABLE'")
            .append("\n      where")
            .append("\n        TABLE_SCHEMA=CURRENT_SCHEMA");
        return sb.toString();
    }

    public String currentUserAndSchema() {
        StringBuilder sb = new StringBuilder()
            .append("\n      select")
            .append("\n      sys_context( 'USERENV', 'CURRENT_SCHEMA' ) CURRENT_SCHEMA,")
            .append("\n      sys_context( 'USERENV', 'CURRENT_USER') CURRENT_USER")
            .append("\n      from dual");
        return sb.toString();
    }

    public String count101(String tableName, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT COUNT(*) FROM (")
            .append("\n        SELECT 1 FROM ");
        sb.append(tableName);
        sb.append("\n        WHERE ROWNUM <= 100");
        if (whereClause != null) {
        sb.append("\n\n            AND ");
        sb.append(whereClause);
        }
        sb.append("\n\n      ) SUBTABLE");
        return sb.toString();
    }

    public String queryWithSystemColumns(String subquery) {
        StringBuilder sb = new StringBuilder()
            .append("\n\n      SELECT")
            .append("\n      row_number() OVER (order by null) \"PLAYSQL_ID\",")
            .append("\n      row_number() OVER (order by null) \"PLAYSQL_POSITION\",")
            .append("\n      subquery.*")
            .append("\n      FROM ( ");
        sb.append(subquery);
        sb.append(" ) subquery");
        return sb.toString();
    }

    public String readData(String fieldlist, 
                           Long limit, 
                           Long offset, 
                           String order, 
                           String tablename, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ");
        sb.append(fieldlist);
        sb.append("\n      FROM ");
        sb.append(tablename);
        sb.append(" AS TARGET_TABLE");
        if (whereClause != null) {
        sb.append("\n WHERE ");
        sb.append(whereClause);
        }
        if (order != null) {
        sb.append("\n ORDER BY ");
        sb.append(order);
        }
        if (limit != null) {
        sb.append("\n LIMIT ");
        sb.append(limit);
        }
        if (offset != null) {
        sb.append("\n OFFSET ");
        sb.append(offset);
        }
        return sb.toString();
    }

    public String writeData(List<String> fieldlist, 
                            Long pkvalueint, 
                            String tableName) {
        StringBuilder sb = new StringBuilder()
            .append("\n\n      UPDATE ");
        sb.append(tableName);
        sb.append("\n      SET ");
        for (int i = 0; i < fieldlist.size(); i++) {
            Object field = fieldlist.get(i);
        if (i != 0) {
        sb.append(" , ");
        }
        sb.append(field);
        sb.append("\n = ? ");
        }
        sb.append("\n      WHERE \"ID\" = ");
        sb.append(pkvalueint);
        return sb.toString();
    }

    public String insertData(List<String> fieldlist, 
                             String tablename) {
        StringBuilder sb = new StringBuilder()
            .append("\n      INSERT INTO ");
        sb.append(tablename);
        sb.append(" (");
        for (int i = 0; i < fieldlist.size(); i++) {
            Object field = fieldlist.get(i);
        if (i != 0) {
        sb.append("\n\n , ");
        }
        sb.append(" \"");
        sb.append(field);
        sb.append("\"");
        }
        sb.append("\n\n      ) VALUES (");
        for (int i = 0; i < fieldlist.size(); i++) {
            Object field = fieldlist.get(i);
        if (i != 0) {
        sb.append("\n\n , ");
        }
        sb.append(" ?");
        }
        sb.append("\n\n      )");
        return sb.toString();
    }

    public String getLastGeneratedId() {
        StringBuilder sb = new StringBuilder().append("select IDENTITY()");
        return sb.toString();
    }

    public String removeData(Integer pkvalueint, 
                             String tablename) {
        StringBuilder sb = new StringBuilder()
            .append("\n            DELETE FROM \"");
        sb.append(tablename);
        sb.append("\"");
        if (pkvalueint != null) {
        sb.append("\n\n              WHERE \"ID\" = ");
        sb.append(pkvalueint);
        }
        return sb.toString();
    }

    public String queryTotals(List<String> fieldlist, 
                              String subquery, 
                              String tablename) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ");
        for (int i = 0; i < fieldlist.size(); i++) {
            Object fieldDef = fieldlist.get(i);
        if (i != 0) {
        sb.append(" , ");
        }
        sb.append(fieldDef);
        }
        sb.append("\n\n\n      FROM");
        if (subquery != null) {
        sb.append("\n ( ");
        sb.append(subquery);
        sb.append("\n        ) INTERNAL_RESULTS");
        }
        if (subquery == null) {
        sb.append(tablename);
        }
        return sb.toString();
    }

    public String hasTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n        select COUNT(*)")
            .append("\n        from  INFORMATION_SCHEMA.TABLES")
            .append("\n        where LOWER(TABLE_NAME) = LOWER(?)")
            .append("\n              and TABLE_SCHEMA = CURRENT_SCHEMA");
        return sb.toString();
    }

    public String getCommentOnTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n        select")
            .append("\n              T.TABLE_NAME TABLENAME,")
            .append("\n              C.COMMENT COM")
            .append("\n        from")
            .append("\n              INFORMATION_SCHEMA.TABLES T")
            .append("\n        left join")
            .append("\n              INFORMATION_SCHEMA.SYSTEM_COMMENTS C")
            .append("\n              on T.TABLE_CATALOG = C.OBJECT_CATALOG")
            .append("\n              and T.TABLE_SCHEMA = C.OBJECT_SCHEMA")
            .append("\n              and T.TABLE_NAME = C.OBJECT_NAME")
            .append("\n              and C.OBJECT_TYPE='TABLE'")
            .append("\n        where")
            .append("\n              TABLE_SCHEMA=CURRENT_SCHEMA")
            .append("\n              and TABLE_NAME = ?");
        return sb.toString();
    }

    public String getCommentOnColumn() {
        StringBuilder sb = new StringBuilder()
            .append("\n        select")
            .append("\n              COMMENT COM")
            .append("\n        from")
            .append("\n              INFORMATION_SCHEMA.SYSTEM_COMMENTS C")
            .append("\n        where")
            .append("\n              TABLE_SCHEMA = CURRENT_SCHEMA")
            .append("\n              and TABLE_NAME = ?")
            .append("\n              and COLUMN_NAME = ?")
            .append("\n              and OBJECT_TYPE = 'COLUMN'");
        return sb.toString();
    }

    public String setCommentOnTable(Object comment, 
                                    Object tablename) {
        StringBuilder sb = new StringBuilder().append("COMMENT ON TABLE \"");
        sb.append(tablename);
        sb.append("\" IS ");
        sb.append(comment);
        return sb.toString();
    }

    public String createTable(List<String> columns, 
                              Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        CREATE TABLE \"");
        sb.append(table);
        sb.append("\" (");
        for (int i = 0; i < columns.size(); i++) {
            Object column = columns.get(i);
        if (i != 0) {
        sb.append("\n\n , ");
        }
        sb.append(column);
        }
        sb.append("\n\n\n        )");
        return sb.toString();
    }

    public String renameTable(Object newName, 
                              Object table) {
        StringBuilder sb = new StringBuilder().append("ALTER TABLE \"");
        sb.append(table);
        sb.append("\" RENAME TO \"");
        sb.append(newName);
        sb.append("\"");
        return sb.toString();
    }

    public String copyTable(Object origin, 
                            Object table) {
        StringBuilder sb = new StringBuilder().append("SELECT * INTO \"");
        sb.append(table);
        sb.append("\" FROM \"");
        sb.append(origin);
        sb.append("\"");
        return sb.toString();
    }

    public String getMaxId(Object table) {
        StringBuilder sb = new StringBuilder().append("SELECT MAX(\"ID\") + 1 FROM \"");
        sb.append(table);
        sb.append("\"");
        return sb.toString();
    }

    public String createSequence(Integer maxId, 
                                 String seqName, 
                                 Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        CREATE SEQUENCE \"");
        sb.append(seqName);
        sb.append("\" START WITH ");
        sb.append(maxId);
        sb.append(";")
            .append("\n        ALTER TABLE \"");
        sb.append(table);
        sb.append("\" ALTER COLUMN \"ID\" SET DEFAULT nextval('\"");
        sb.append(seqName);
        sb.append("\"')");
        return sb.toString();
    }

    public String dropTable(Object dropBehaviour, 
                            boolean ifExists, 
                            Object table) {
        StringBuilder sb = new StringBuilder();
            /**
             *  DROP TABLE [ IF EXISTS ] <table name> [ IF EXISTS ] <drop behavior> 
            **/
        sb.append("\n\n    DROP TABLE \"");
        sb.append(table);
        sb.append("\"");
        if (ifExists) {
        sb.append("\n IF EXISTS ");
        }
        sb.append(dropBehaviour);
        return sb.toString();
    }

    public String addColumn(String beforeColumn, 
                            String columnDefinition, 
                            String table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        ALTER TABLE ");
        sb.append(table);
        sb.append("\n        ADD COLUMN ");
        sb.append(columnDefinition);
        if (beforeColumn != null) {
        sb.append("\n BEFORE ");
        sb.append(beforeColumn);
        }
        return sb.toString();
    }

    public String dropColumn(Object behaviour, 
                             Object column, 
                             Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        ALTER TABLE \"");
        sb.append(table);
        sb.append("\"")
            .append("\n        DROP COLUMN \"");
        sb.append(column);
        sb.append("\"");
        sb.append(behaviour);
        return sb.toString();
    }

    public String renameColumn(Object newColumn, 
                               Object originalColumn, 
                               Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        ALTER TABLE \"");
        sb.append(table);
        sb.append("\"")
            .append("\n        RENAME COLUMN \"");
        sb.append(originalColumn);
        sb.append("\"")
            .append("\n        TO \"");
        sb.append(newColumn);
        sb.append("\"");
        return sb.toString();
    }

    public String alterColumn(String columnDefinition, 
                              String table) {
        StringBuilder sb = new StringBuilder()
            .append("\n        ALTER TABLE ");
        sb.append(table);
        sb.append("\n        ALTER COLUMN ");
        sb.append(columnDefinition);
        return sb.toString();
    }

    public String setCommentOnColumn(Object columnname, 
                                     Object comment, 
                                     Object tablename) {
        StringBuilder sb = new StringBuilder()
            .append("\n        COMMENT ON COLUMN ");
        sb.append(tablename);
        sb.append(".");
        sb.append(columnname);
        sb.append(" IS ");
        sb.append(comment);
        return sb.toString();
    }

    public String fkAddToColumn(Object column, 
                                Object fkDefinition, 
                                Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n      ALTER TABLE ");
        sb.append(table);
        sb.append("\n      ADD FOREIGN KEY (");
        sb.append(column);
        sb.append(") ");
        sb.append(fkDefinition);
        return sb.toString();
    }

    public String fkRemove(Object constraintName, 
                           Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n      ALTER TABLE ");
        sb.append(table);
        sb.append("\n      DROP CONSTRAINT ");
        sb.append(constraintName);
        return sb.toString();
    }

    public String listFksForTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT")
            .append("\n            tc.constraint_name,")
            .append("\n            tc.table_name,")
            .append("\n            kcu.column_name,")
            .append("\n            ccu.table_name AS foreign_table_name,")
            .append("\n            ccu.column_name AS foreign_column_name,")
            .append("\n            pgc.confupdtype, ")
            .append("\n            pgc.confdeltype,")
            .append("\n            pgc.confmatchtype ")
            .append("\n        FROM")
            .append("\n            information_schema.table_constraints AS tc")
            .append("\n        JOIN information_schema.key_column_usage AS kcu")
            .append("\n          ON tc.constraint_name = kcu.constraint_name")
            .append("\n        JOIN information_schema.constraint_column_usage AS ccu")
            .append("\n          ON ccu.constraint_name = tc.constraint_name")
            .append("\n        JOIN pg_catalog.pg_constraint AS pgc")
            .append("\n          ON pgc.conname = tc.constraint_name")
            .append("\n        WHERE tc.table_name = ? AND tc.constraint_type = 'FOREIGN KEY'");
        return sb.toString();
    }

    public String fkTestValueMatch(Object column, 
                                   Object table, 
                                   Object targetColumn, 
                                   Object targetTable) {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT DISTINCT ");
        sb.append(column);
        sb.append("\n        FROM ");
        sb.append(table);
        sb.append("\n        WHERE ");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(" NOT IN (SELECT ");
        sb.append(targetColumn);
        sb.append(" FROM ");
        sb.append(targetTable);
        sb.append(")")
            .append("\n        LIMIT 10");
        return sb.toString();
    }

    public String fkInsertMissingValues(Object column, 
                                        Object table, 
                                        Object targetColumn, 
                                        Object targetTable) {
        StringBuilder sb = new StringBuilder()
            .append("\n        INSERT INTO ");
        sb.append(targetTable);
        sb.append(" (");
        sb.append(targetColumn);
        sb.append(")")
            .append("\n        SELECT DISTINCT ");
        sb.append(column);
        sb.append("\n        FROM ");
        sb.append(table);
        sb.append("\n        WHERE ");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(" NOT IN (SELECT ");
        sb.append(targetColumn);
        sb.append(" FROM ");
        sb.append(targetTable);
        sb.append(" WHERE ");
        sb.append(targetColumn);
        sb.append(" IS NOT NULL)")
            .append("\n          AND ");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(" IS NOT NULL")
            .append("\n        ORDER BY ");
        sb.append(column);
        sb.append(" ASC");
        return sb.toString();
    }

    public String fkInsertMissingValuesInPk(boolean addInfoInDisplayColumn, 
                                            Object column, 
                                            Object displayColumn, 
                                            Object idColumn, 
                                            Object table, 
                                            Object targetTable) {
        StringBuilder sb = new StringBuilder()
            .append("\n        INSERT INTO ");
        sb.append(targetTable);
        sb.append(" (");
        sb.append(idColumn);
        if (addInfoInDisplayColumn) {
        sb.append("\n, ");
        sb.append(displayColumn);
        }
        sb.append("\n        )")
            .append("\n        SELECT DISTINCT ");
        sb.append(column);
        if (addInfoInDisplayColumn) {
        sb.append("\n , '#' || ");
        sb.append(column);
        }
        sb.append("\n        FROM ");
        sb.append(table);
        sb.append("\n        WHERE ");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(" NOT IN (SELECT ");
        sb.append(idColumn);
        sb.append(" FROM ");
        sb.append(targetTable);
        sb.append(" WHERE ");
        sb.append(idColumn);
        sb.append(" IS NOT NULL)")
            .append("\n          AND ");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(" IS NOT NULL")
            .append("\n        ORDER BY ");
        sb.append(column);
        sb.append(" ASC");
        return sb.toString();
    }

    public String fkTestValueMatchWithNumericCast(Object column, 
                                                  Object table, 
                                                  Object targetColumn, 
                                                  Object targetTable) {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT DISTINCT ");
        sb.append(column);
        sb.append("\n        FROM ");
        sb.append(table);
        sb.append("\n        WHERE")
            .append("\n        CASE WHEN ");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(" ~ '^ *[0-9]+ *$'")
            .append("\n             THEN")
            .append("\n\n                CAST(TRIM(");
        sb.append(table);
        sb.append(".");
        sb.append(column);
        sb.append(") AS INTEGER)")
            .append("\n                    NOT IN")
            .append("\n                        (SELECT ");
        sb.append(targetColumn);
        sb.append(" FROM ");
        sb.append(targetTable);
        sb.append(")")
            .append("\n        ELSE")
            .append("\n            TRUE ")
            .append("\n        END")
            .append("\n        LIMIT 10");
        return sb.toString();
    }

    public String fkCopyAndTransformValues(Object column, 
                                           Object oldColumn, 
                                           Object table, 
                                           Object targetColumn, 
                                           Object targetTable) {
        StringBuilder sb = new StringBuilder()
            .append("\n    UPDATE ");
        sb.append(table);
        sb.append("\n    SET ");
        sb.append(column);
        sb.append(" = (SELECT \"ID\" FROM ");
        sb.append(targetTable);
        sb.append(" WHERE ");
        sb.append(targetColumn);
        sb.append(" = ");
        sb.append(oldColumn);
        sb.append(")")
            .append("\n    WHERE ");
        sb.append(oldColumn);
        sb.append(" IS NOT NULL");
        return sb.toString();
    }

    public String listColumns() {
        StringBuilder sb = new StringBuilder()
            .append("\n        select")
            .append("\n              T.COLUMN_NAME, T.DATA_TYPE,")
            .append("\n              T.COLUMN_DEFAULT, T.IS_NULLABLE, T.IS_GENERATED,")
            .append("\n              T.IS_UPDATABLE, T.IS_IDENTITY,")
            .append("\n              T.CHARACTER_MAXIMUM_LENGTH,")
            .append("\n              T.NUMERIC_PRECISION, T.NUMERIC_SCALE,")
            .append("\n              T.DATETIME_PRECISION,")
            .append("\n              T.INTERVAL_TYPE, T.INTERVAL_PRECISION,")
            .append("\n              C.COMMENT COM")
            .append("\n        from")
            .append("\n              INFORMATION_SCHEMA.COLUMNS T")
            .append("\n        left join")
            .append("\n              INFORMATION_SCHEMA.SYSTEM_COMMENTS C")
            .append("\n              on T.TABLE_CATALOG = C.OBJECT_CATALOG")
            .append("\n              and T.TABLE_SCHEMA = C.OBJECT_SCHEMA")
            .append("\n              and T.TABLE_NAME = C.OBJECT_NAME")
            .append("\n              and T.COLUMN_NAME = C.COLUMN_NAME")
            .append("\n              and C.OBJECT_TYPE='COLUMN'")
            .append("\n        where")
            .append("\n              T.TABLE_NAME = ?")
            .append("\n              and T.TABLE_SCHEMA = CURRENT_SCHEMA")
            .append("\n        order by")
            .append("\n              T.ORDINAL_POSITION");
        return sb.toString();
    }

    public String selectTable(Object table) {
        StringBuilder sb = new StringBuilder().append("SELECT * FROM ");
        sb.append(table);
        sb.append(" LIMIT 20");
        return sb.toString();
    }

    public String createSettingsTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE TABLE playsql_settings")
            .append("\n            (")
            .append("\n                id serial primary key,")
            .append("\n                category text,")
            .append("\n                key text,")
            .append("\n                value text")
            .append("\n            )");
        return sb.toString();
    }

    public String getSettingsWithoutCategory() {
        StringBuilder sb = new StringBuilder().append("SELECT value FROM playsql_settings WHERE key = ? AND category IS NULL");
        return sb.toString();
    }

    public String getSettingsWithCategory() {
        StringBuilder sb = new StringBuilder().append("SELECT value FROM playsql_settings WHERE key = ? AND category = ?");
        return sb.toString();
    }

    public String deleteSettings() {
        StringBuilder sb = new StringBuilder().append("DELETE FROM playsql_settings WHERE key = ?");
        return sb.toString();
    }

    public String insertSettings() {
        StringBuilder sb = new StringBuilder().append("INSERT INTO playsql_settings (category, key, value) VALUES (?, ?, ?)");
        return sb.toString();
    }

    public String createAuditTrailTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n    CREATE TABLE playsql_audit_trail")
            .append("\n    (")
            .append("\n        id serial primary key,")
            .append("\n        operation text,")
            .append("\n        table_name text,")
            .append("\n        column_name text,")
            .append("\n        row_id integer,")
            .append("\n        author text,")
            .append("\n        timestamp timestamp without time zone,")
            .append("\n        comments text,")
            .append("\n        data_type text,")
            .append("\n        old_val text,")
            .append("\n        new_val text")
            .append("\n    )");
        return sb.toString();
    }

    public String readAuditTrail(boolean hasAuthor, 
                                 boolean hasColumnName, 
                                 boolean hasRowID, 
                                 boolean hasTableName, 
                                 boolean includeGeneralActivity, 
                                 Integer limit, 
                                 Integer offset) {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT id, operation,")
            .append("\n               table_name, column_name, row_id,")
            .append("\n               author, timestamp, comments,")
            .append("\n               data_type, old_val, new_val")
            .append("\n        FROM playsql_audit_trail")
            .append("\n        WHERE 1=1");
        if (hasTableName) {
        sb.append("\n AND table_name = ? ");
        }
        if (hasColumnName) {
        sb.append("\n AND column_name = ? ");
        }
        if (hasRowID) {
        sb.append("\n AND (row_id = ? ");
        if (includeGeneralActivity) {
        sb.append(" OR row_id IS NULL ");
        }
        sb.append(" ) ");
        }
        if (hasAuthor) {
        sb.append("\n AND author = ? ");
        }
        if (includeGeneralActivity) {
        sb.append("\n OR table_name IS NULL ");
        }
        sb.append("\n        ORDER BY timestamp DESC");
        if (limit != null) {
        sb.append("\n LIMIT ");
        sb.append(limit);
        }
        if (offset != null) {
        sb.append("\n OFFSET ");
        sb.append(offset);
        }
        return sb.toString();
    }

    public String insertAuditTrail() {
        StringBuilder sb = new StringBuilder()
            .append("\n        INSERT INTO playsql_audit_trail")
            .append("\n              (operation,")
            .append("\n               table_name, column_name, row_id,")
            .append("\n               author, timestamp, comments,")
            .append("\n               data_type, old_val, new_val)")
            .append("\n        VALUES (?,")
            .append("\n                ?, ?, ?,")
            .append("\n                ?, ?, ?,")
            .append("\n                ?, ?, ?)");
        return sb.toString();
    }

    public String auditTrailTrimByDaysPrerequisites() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT id, operation,")
            .append("\n                table_name, column_name, row_id,")
            .append("\n                author, timestamp, comments,")
            .append("\n                data_type, old_val, new_val")
            .append("\n            FROM playsql_audit_trail")
            .append("\n            WHERE timestamp < NOW() - (INTERVAL '1 DAYS' * ?)")
            .append("\n            AND (operation = 'COL_DROP' OR operation = 'TABLE_DROP')")
            .append("\n            ORDER BY timestamp DESC ");
            /**
             *  We want table drops before column drops 
            **/
        return sb.toString();
    }

    public String auditTrailTrimByDays() {
        StringBuilder sb = new StringBuilder()
            .append("\n            DELETE FROM playsql_audit_trail")
            .append("\n            WHERE timestamp < NOW() - (INTERVAL '1 DAYS' * ?)")
            .append("\n            AND operation <> 'ACTIVATION'");
        return sb.toString();
    }

    public String auditTrailTrimByCount1() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT table_name")
            .append("\n            FROM playsql_audit_trail")
            .append("\n            GROUP BY table_name")
            .append("\n            HAVING COUNT(*) >= ?");
        return sb.toString();
    }

    public String auditTrailTrimByCountPrerequisites() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT id, operation,")
            .append("\n                table_name, column_name, row_id,")
            .append("\n                author, timestamp, comments,")
            .append("\n                data_type, old_val, new_val")
            .append("\n            FROM playsql_audit_trail")
            .append("\n            WHERE id IN (SELECT id FROM playsql_audit_trail WHERE table_name = ? ORDER BY timestamp DESC OFFSET ?)")
            .append("\n            AND (operation = 'COL_DROP' OR operation = 'TABLE_DROP')");
        return sb.toString();
    }

    public String auditTrailTrimByCount2() {
        StringBuilder sb = new StringBuilder()
            .append("\n            DELETE FROM playsql_audit_trail")
            .append("\n            WHERE id IN (SELECT id FROM playsql_audit_trail WHERE table_name = ? ORDER BY timestamp DESC OFFSET ?)");
        return sb.toString();
    }

    public String createFormulaDependenciesTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE TABLE playsql_formula_dependencies")
            .append("\n            (")
            .append("\n                id serial PRIMARY KEY,")
            .append("\n                origin_tablename text,")
            .append("\n                origin_columnname text,")
            .append("\n                origin_rowid integer,")
            .append("\n                destination_tablename text,")
            .append("\n                destination_columnname text,")
            .append("\n                destination_rowid integer,")
            .append("\n                formula text")
            .append("\n            )");
        return sb.toString();
    }

    public String insertFormulaDependency() {
        StringBuilder sb = new StringBuilder()
            .append("\n            INSERT INTO playsql_formula_dependencies")
            .append("\n            ( origin_tablename, origin_columnname, origin_rowid,")
            .append("\n              destination_tablename, destination_columnname, destination_rowid,")
            .append("\n              formula)")
            .append("\n            VALUES ( ?, ?, ?,")
            .append("\n                     ?, ?, ?,")
            .append("\n                     ?)");
        return sb.toString();
    }

    public String listFormulaDependencies(long MAX_RECURSION, 
                                          boolean cascade, 
                                          boolean colNameIsNull, 
                                          boolean includeSelf, 
                                          boolean rowIdIsNull, 
                                          boolean tableNameIsNull) {
        StringBuilder sb = new StringBuilder();
        if (cascade) {
        sb.append("\n\n            WITH RECURSIVE deps(tablename, columnname, rowid, formula, level) AS (")
            .append("\n\n                SELECT destination_tablename, destination_columnname, destination_rowid, formula, 1 AS level")
            .append("\n                FROM playsql_formula_dependencies")
            .append("\n                WHERE 1=1");
        if (!tableNameIsNull) {
        sb.append("\n AND origin_tablename = ? ");
        }
        if (!colNameIsNull) {
        sb.append("\n AND (origin_columnname = ? OR origin_columnname IS NULL) ");
        }
        if (!rowIdIsNull) {
        sb.append("\n AND (origin_rowid = ? OR origin_rowid IS NULL) ");
        }
        sb.append("\n\n            UNION ALL")
            .append("\n                SELECT")
            .append("\n                deps2.destination_tablename, deps2.destination_columnname, deps2.destination_rowid, deps2.formula, deps.level+1")
            .append("\n                FROM playsql_formula_dependencies deps2")
            .append("\n                JOIN deps ON deps2.origin_tablename = deps.tablename")
            .append("\n                AND (deps2.origin_columnname = deps.columnname OR deps2.origin_columnname IS NULL)")
            .append("\n                AND (deps2.origin_rowid = deps.rowid OR deps2.origin_rowid IS NULL)")
            .append("\n                AND level <= ");
        sb.append(MAX_RECURSION);
        sb.append("\n\n            )")
            .append("\n            SELECT DISTINCT tablename, columnname, rowid, formula, MAX(level) AS level FROM deps")
            .append("\n            GROUP BY tablename, columnname, rowid, formula");
        if (includeSelf) {
        sb.append("\n\n\n                UNION ALL")
            .append("\n\n                SELECT DISTINCT")
            .append("\n                    destination_tablename tablename, destination_columnname columnname, destination_rowid rowid, formula, 0 AS level")
            .append("\n                FROM")
            .append("\n                    playsql_formula_dependencies")
            .append("\n                WHERE 1=1");
        if (!tableNameIsNull) {
        sb.append("\n AND destination_tablename = ? ");
        }
        if (!colNameIsNull) {
        sb.append("\n AND destination_columnname = ? ");
        }
        if (!rowIdIsNull) {
        sb.append("\n AND destination_rowid = ? ");
        }
        }
        sb.append("\n\n\n            ORDER BY level ASC");
        }
        if (!cascade) {
        sb.append("\n\n\n\n            SELECT DISTINCT")
            .append("\n                destination_tablename tablename, destination_columnname columnname, destination_rowid rowid, formula, 1 AS level")
            .append("\n            FROM")
            .append("\n                playsql_formula_dependencies")
            .append("\n            WHERE 1=1");
        if (!tableNameIsNull) {
        sb.append("\n AND origin_tablename = ? ");
        }
        if (!colNameIsNull) {
        sb.append("\n AND (origin_columnname = ? OR origin_columnname IS NULL) ");
        }
        if (!rowIdIsNull) {
        sb.append("\n AND (origin_rowid = ? OR origin_rowid IS NULL) ");
        }
        }
        return sb.toString();
    }

    public String formulaDependendenciesFindLoop(long MAX_RECURSION, 
                                                 boolean cascade, 
                                                 boolean colNameIsNull, 
                                                 boolean rowIdIsNull, 
                                                 boolean tableNameIsNull) {
        StringBuilder sb = new StringBuilder()
            .append("\n\n            -- This query is in charge of searching for recursive loops. It stores each step in an array (\"path\")")
            .append("\n            -- then the 'loop_detected' column tells uses the x==ANY(array) condition to check for presence")
            .append("\n            -- of the current formula")
            .append("\n            WITH RECURSIVE deps(tablename, columnname, rowid, formula, level, path, loop_detected) AS (")
            .append("\n\n                SELECT")
            .append("\n                    destination_tablename, destination_columnname, destination_rowid, formula,")
            .append("\n                    1 AS level,")
            .append("\n                    ARRAY[destination_tablename || '.' || destination_columnname || ':' || destination_rowid] path,")
            .append("\n                    FALSE loop_detected")
            .append("\n                FROM playsql_formula_dependencies")
            .append("\n                WHERE 1=1");
        if (!tableNameIsNull) {
        sb.append("\n AND origin_tablename = ? ");
        }
        if (!colNameIsNull) {
        sb.append("\n AND (origin_columnname = ? OR origin_columnname IS NULL) ");
        }
        if (!rowIdIsNull) {
        sb.append("\n AND (origin_rowid = ? OR origin_rowid IS NULL) ");
        }
        sb.append("\n\n            UNION ALL")
            .append("\n                SELECT")
            .append("\n                    deps2.destination_tablename, deps2.destination_columnname, deps2.destination_rowid, deps2.formula,")
            .append("\n                    deps.level+1,")
            .append("\n                    path || (deps2.destination_tablename || '.' || deps2.destination_columnname || ':' || deps2.destination_rowid),")
            .append("\n                    ((deps2.destination_tablename || '.' || deps2.destination_columnname || ':' || deps2.destination_rowid) = ANY(path)) loop_detected")
            .append("\n                FROM playsql_formula_dependencies deps2")
            .append("\n                JOIN deps ON deps2.origin_tablename = deps.tablename")
            .append("\n                AND (deps2.origin_columnname = deps.columnname OR deps2.origin_columnname IS NULL)")
            .append("\n                AND (deps2.origin_rowid = deps.rowid OR deps2.origin_rowid IS NULL)")
            .append("\n                AND level <= ");
        sb.append(MAX_RECURSION);
        sb.append("\n\n            )")
            .append("\n            SELECT formula, level, loop_detected, ARRAY_TO_STRING(path, ' -> ') path")
            .append("\n            FROM deps")
            .append("\n            WHERE loop_detected")
            .append("\n            LIMIT 1");
        return sb.toString();
    }

    public String removeFormulaDependencies(boolean colNameIsNull, 
                                            boolean rowIdIsNull, 
                                            boolean tableNameIsNull) {
        StringBuilder sb = new StringBuilder()
            .append("\n            DELETE")
            .append("\n            FROM")
            .append("\n                playsql_formula_dependencies")
            .append("\n            WHERE 1=1");
        if (!tableNameIsNull) {
        sb.append("\n AND destination_tablename = ? ");
        }
        if (!colNameIsNull) {
        sb.append("\n AND (destination_columnname = ? OR destination_columnname IS NULL) ");
        }
        if (!rowIdIsNull) {
        sb.append("\n AND (destination_rowid = ? OR destination_rowid IS NULL) ");
        }
        return sb.toString();
    }

    public String executeFormula(Object columnName, 
                                 Object formula, 
                                 Object tableName) {
        StringBuilder sb = new StringBuilder()
            .append("\n            UPDATE ");
        sb.append(tableName);
        sb.append("\n            SET ");
        sb.append(columnName);
        sb.append(" = (");
        sb.append(formula);
        sb.append(")")
            .append("\n            WHERE \"ID\" = ?");
        return sb.toString();
    }

    public String updateFormula() {
        StringBuilder sb = new StringBuilder()
            .append("\n            UPDATE playsql_formula_dependencies")
            .append("\n            SET formula = ?")
            .append("\n            WHERE destination_tablename = ?")
            .append("\n            AND destination_columnname = ?")
            .append("\n            AND destination_rowid = ?");
        return sb.toString();
    }

    public String updateFormulaRenameColumnOrigin() {
        StringBuilder sb = new StringBuilder()
            .append("\n            UPDATE playsql_formula_dependencies")
            .append("\n            SET origin_columnname = ?")
            .append("\n            WHERE origin_tablename = ?")
            .append("\n            AND origin_columnname = ?");
        return sb.toString();
    }

    public String updateFormulaRenameColumnDestination() {
        StringBuilder sb = new StringBuilder()
            .append("\n            UPDATE playsql_formula_dependencies")
            .append("\n            SET destination_columnname = ?")
            .append("\n            WHERE destination_tablename = ?")
            .append("\n            AND destination_columnname = ?");
        return sb.toString();
    }

    public String createEntitiesTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE SEQUENCE playsql_entities_id_seq")
            .append("\n            START WITH 1000;")
            .append("\n            CREATE TABLE playsql_entities")
            .append("\n            (")
            .append("\n                id integer NOT NULL DEFAULT nextval('playsql_entities_id_seq') PRIMARY KEY,")
            .append("\n                entity_type text,")
            .append("\n                key text,")
            .append("\n                label text,")
            .append("\n                last_modified text,")
            .append("\n                sql text,")
            .append("\n                serialized text")
            .append("\n            );")
            .append("\n            ALTER SEQUENCE playsql_entities_id_seq OWNED BY playsql_entities.id;");
        return sb.toString();
    }

    public String insertEntity() {
        StringBuilder sb = new StringBuilder()
            .append("\n            INSERT INTO playsql_entities (entity_type, key, label, last_modified, sql, serialized)")
            .append("\n            VALUES (?, ?, ?, ?, ?, ?)");
        return sb.toString();
    }

    public String readEntity(boolean hasId, 
                             boolean hasKey, 
                             boolean hasType) {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT id, entity_type, label, serialized")
            .append("\n            FROM playsql_entities")
            .append("\n            WHERE");
        if (hasType) {
        sb.append("\n entity_type = ? ");
        } else {
        sb.append(" 1=1 ");
        }
        if (hasId) {
        sb.append("\n AND id = ? ");
        }
        if (hasKey) {
        sb.append("\n AND COALESCE(key, id::text) = ? ");
        }
        return sb.toString();
    }

    public String deleteEntity(boolean hasId, 
                               boolean hasKey, 
                               boolean hasType) {
        StringBuilder sb = new StringBuilder()
            .append("\n            DELETE FROM playsql_entities")
            .append("\n            WHERE");
        if (hasType) {
        sb.append("\n entity_type = ? ");
        } else {
        sb.append(" 1=1 ");
        }
        if (hasId) {
        sb.append("\n AND id = ? ");
        }
        if (hasKey) {
        sb.append("\n AND key = ? ");
        }
        return sb.toString();
    }

    public String updateEntity(boolean hasId, 
                               boolean hasKey, 
                               boolean hasType) {
        StringBuilder sb = new StringBuilder()
            .append("\n            UPDATE playsql_entities")
            .append("\n            SET entity_type = ?,")
            .append("\n                key = ?,")
            .append("\n                label = ?,")
            .append("\n                last_modified = ?,")
            .append("\n                sql = ?,")
            .append("\n                serialized = ?")
            .append("\n            WHERE");
        if (hasType) {
        sb.append("\n type = ? ");
        } else {
        sb.append(" 1=1 ");
        }
        if (hasId) {
        sb.append("\n AND id = ? ");
        }
        if (hasKey) {
        sb.append("\n AND key = ? ");
        }
        return sb.toString();
    }

    public String usersList(boolean detail) {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT rolname username");
        if (detail) {
        sb.append("\n\n            ,")
            .append("\n                   ARRAY(SELECT parent.rolname")
            .append("\n                         FROM pg_roles parent")
            .append("\n                         JOIN pg_auth_members m ON u.oid = m.member")
            .append("\n                         WHERE parent.oid = m.roleid")
            .append("\n                   ) memberships");
        }
        sb.append("\n\n            FROM pg_roles u")
            .append("\n            WHERE rolname LIKE ? || '%'");
        return sb.toString();
    }

    public String userCreate(Object userName, 
                             Object userPassword) {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE USER ");
        sb.append(userName);
        if (userPassword != null) {
        sb.append("\n WITH ENCRYPTED PASSWORD ");
        sb.append(userPassword);
        }
        return sb.toString();
    }

    public String userUpdatePassword(Object userName, 
                                     Object userPassword) {
        StringBuilder sb = new StringBuilder()
            .append("\n            ALTER USER ");
        sb.append(userName);
        sb.append("\n            WITH ENCRYPTED PASSWORD ");
        sb.append(userPassword);
        return sb.toString();
    }

    public String userExists() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT COUNT(*) FROM pg_user WHERE usename = ?");
        return sb.toString();
    }

    public String userDrop(Object userName) {
        StringBuilder sb = new StringBuilder()
            .append("\n            DROP USER ");
        sb.append(userName);
        return sb.toString();
    }

    public String databaseCreate(Object dbName) {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE DATABASE ");
        sb.append(dbName);
        return sb.toString();
    }

    public String databaseExists() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT COUNT(*) FROM PG_DATABASE WHERE datname = ?");
        return sb.toString();
    }

    public String databaseDrop(Object dbName) {
        StringBuilder sb = new StringBuilder()
            .append("\n            DROP DATABASE ");
        sb.append(dbName);
        return sb.toString();
    }

    public String databaseGrantAll(Object dbName, 
                                   Object dbUser) {
        StringBuilder sb = new StringBuilder()
            .append("\n            GRANT ALL PRIVILEGES ON DATABASE ");
        sb.append(dbName);
        sb.append("\n            TO ");
        sb.append(dbUser);
        return sb.toString();
    }

    public String databaseList() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT")
            .append("\n                datname AS database_name,")
            .append("\n                pg_database_size(datname) AS size,")
            .append("\n                (SELECT usename FROM pg_catalog.pg_user u WHERE u.usesysid = d.datdba LIMIT 1) AS owner,")
            .append("\n                datallowconn,")
            .append("\n                datconnlimit,")
            .append("\n                datacl permissions")
            .append("\n            FROM pg_catalog.pg_database d")
            .append("\n            ORDER BY size DESC;");
        return sb.toString();
    }

    public String databaseSetAllowcon(String dbName, 
                                      boolean status) {
        StringBuilder sb = new StringBuilder()
            .append("\n            UPDATE pg_catalog.pg_database")
            .append("\n            SET datallowconn = ");
        sb.append(status);
        sb.append("\n            WHERE datname = ");
        sb.append(dbName);
        sb.append(";");
        return sb.toString();
    }


}
