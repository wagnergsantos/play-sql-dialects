/*
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
*/

package com.playsql.jdbc.dialect.templates;

import java.util.List;

public class TemplatesForOracle extends TemplatesForGeneric {


    @Override
    public String selectTable(Object table) {
        StringBuilder sb = new StringBuilder().append("SELECT * FROM ");
        sb.append(table);
        sb.append(" WHERE ROWNUM < 20");
        return sb.toString();
    }


    @Override
    public String readData(String fieldlist, 
                           Long limit, 
                           Long offset, 
                           String order, 
                           String tablename, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT ");
        sb.append(fieldlist);
        sb.append("\n            FROM ");
        sb.append(tablename);
        sb.append(" TARGET_TABLE");
        if (whereClause != null) {
        sb.append("\n WHERE ");
        sb.append(whereClause);
        }
        if (limit != null) {
        sb.append("\n WHERE ROWNUM &lt; ");
        sb.append(limit);
        }
        sb.append("\n@").append("comment We omit the 'offset' because Oracle doesn't support it");
        if (order != null) {
        sb.append("\n ORDER BY ");
        sb.append(order);
        }
        return sb.toString();
    }


}
