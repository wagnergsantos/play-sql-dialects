/*
  #%L
  Play SQL Dialects
  %%
  Copyright (C) 2013 - 2016 Play SQL S.A.S.U.
  %%
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  #L%
*/


package com.playsql.jdbc.dialect.templates;

import java.util.List;

public class TemplatesForHsql extends TemplatesForGeneric {


    @Override
    public String hasSchema() {
        StringBuilder sb = new StringBuilder().append("SELECT COUNT(*) FROM information_schema.schemata WHERE schema_name = ?");
        return sb.toString();
    }


    @Override
    public String getDefaultSchemas() {
        StringBuilder sb = new StringBuilder().append("SELECT CURRENT_SCHEMA FROM (VALUES(1))");
        return sb.toString();
    }


    @Override
    public String setDefaultSchemas(Object schemaName) {
        StringBuilder sb = new StringBuilder().append("SET SCHEMA \"");
        sb.append(schemaName);
        sb.append("\"");
        return sb.toString();
    }

    public String listTablesWithoutSchema(boolean startingWith) {
        StringBuilder sb = new StringBuilder()
            .append("\n      select TABLE_NAME")
            .append("\n      from INFORMATION_SCHEMA.TABLES")
            .append("\n      where UPPER(TABLE_SCHEMA)=UPPER(CURRENT_SCHEMA)");
        if (startingWith) {
        sb.append("\n\n          and UPPER(TABLE_NAME) LIKE (?)");
        }
        return sb.toString();
    }


    @Override
    public String renameColumn(Object newColumn, 
                               Object originalColumn, 
                               Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n      ALTER TABLE \"");
        sb.append(table);
        sb.append("\"")
            .append("\n      ALTER COLUMN \"");
        sb.append(originalColumn);
        sb.append("\"")
            .append("\n      RENAME TO \"");
        sb.append(newColumn);
        sb.append("\"");
        return sb.toString();
    }

    public String emptyStringToNull(Object column, 
                                    Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n      UPDATE \"");
        sb.append(table);
        sb.append("\"")
            .append("\n      SET \"");
        sb.append(column);
        sb.append("\" = NULL")
            .append("\n      WHERE \"");
        sb.append(column);
        sb.append("\" = ''");
        return sb.toString();
    }


    @Override
    public String copyTable(Object origin, 
                            Object table) {
        StringBuilder sb = new StringBuilder()
            .append("\n            -- Creates a table by copying all metadata")
            .append("\n            CREATE TABLE \"");
        sb.append(table);
        sb.append("\" (")
            .append("\n                LIKE \"");
        sb.append(origin);
        sb.append("\"")
            .append("\n                INCLUDING IDENTITY")
            .append("\n                INCLUDING DEFAULTS")
            .append("\n                INCLUDING GENERATED );");
        return sb.toString();
    }

    public String copyTable2(Object origin, 
                             Object table) {
        StringBuilder sb = new StringBuilder().append("INSERT INTO \"");
        sb.append(table);
        sb.append("\" ( SELECT * FROM \"");
        sb.append(origin);
        sb.append("\" );");
        return sb.toString();
    }


    @Override
    public String hasTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES")
            .append("\n        WHERE LOWER(TABLE_NAME) = LOWER(?)")
            .append("\n        AND TABLE_SCHEMA = current_schema");
        return sb.toString();
    }


    @Override
    public String currentUserAndSchema() {
        StringBuilder sb = new StringBuilder()
            .append("\n        SELECT CURRENT_USER, CURRENT_SCHEMA FROM (VALUES(1));");
        return sb.toString();
    }


    @Override
    public String count101(String tableName, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT COUNT(*) FROM (")
            .append("\n      SELECT 1 FROM ");
        sb.append(tableName);
        if (whereClause != null) {
        sb.append("\n\n            WHERE ");
        sb.append(whereClause);
        }
        sb.append("\n\n      LIMIT 101")
            .append("\n      ) SUBTABLE");
        return sb.toString();
    }


    @Override
    public String queryWithSystemColumns(String subquery) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ROWNUM() \"PLAYSQL_ID\", ROWNUM() \"PLAYSQL_POSITION\", subquery.*")
            .append("\n      FROM ( ");
        sb.append(subquery);
        sb.append(" ) subquery");
        return sb.toString();
    }


    @Override
    public String readData(String fieldlist, 
                           Long limit, 
                           Long offset, 
                           String order, 
                           String tablename, 
                           String whereClause) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ");
        sb.append(fieldlist);
        sb.append("\n      FROM ");
        sb.append(tablename);
        sb.append(" TARGET_TABLE");
        if (whereClause != null) {
        sb.append("\n WHERE ");
        sb.append(whereClause);
        }
        if (order != null) {
        sb.append("\n ORDER BY ");
        sb.append(order);
        }
        if (limit != null) {
        sb.append("\n LIMIT ");
        sb.append(limit);
        }
        if (offset != null) {
        sb.append("\n OFFSET ");
        sb.append(offset);
        }
        return sb.toString();
    }


    @Override
    public String queryTotals(List<String> fieldlist, 
                              String subquery, 
                              String tablename) {
        StringBuilder sb = new StringBuilder()
            .append("\n      SELECT ");
        for (int i = 0; i < fieldlist.size(); i++) {
            Object fieldDef = fieldlist.get(i);
        if (i != 0) {
        sb.append(" , ");
        }
        sb.append(fieldDef);
        }
        sb.append("\n      FROM");
        if (subquery != null) {
        sb.append("\n ( ");
        sb.append(subquery);
        sb.append("\n        ) INTERNAL_RESULTS");
        } else {
        sb.append(tablename);
        }
        sb.append("\n\n      LIMIT 1");
        return sb.toString();
    }


    @Override
    public String createSettingsTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE TABLE playsql_settings")
            .append("\n            (")
            .append("\n            id INTEGER GENERATED BY DEFAULT AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY,")
            .append("\n            category LONGVARCHAR,")
            .append("\n            key LONGVARCHAR,")
            .append("\n            value LONGVARCHAR")
            .append("\n            );");
        return sb.toString();
    }


    @Override
    public String createAuditTrailTable() {
        StringBuilder sb = new StringBuilder()
            .append("\n            CREATE TABLE playsql_audit_trail")
            .append("\n            (")
            .append("\n            id INTEGER GENERATED BY DEFAULT AS IDENTITY (START WITH 1, INCREMENT BY 1) PRIMARY KEY,")
            .append("\n            operation LONGVARCHAR,")
            .append("\n            table_name LONGVARCHAR,")
            .append("\n            column_name LONGVARCHAR,")
            .append("\n            row_id INTEGER,")
            .append("\n            author LONGVARCHAR,")
            .append("\n            timestamp TIMESTAMP,")
            .append("\n            comments LONGVARCHAR,")
            .append("\n            data_type LONGVARCHAR,")
            .append("\n            old_val LONGVARCHAR,")
            .append("\n            new_val LONGVARCHAR")
            .append("\n            );");
        return sb.toString();
    }


    @Override
    public String auditTrailTrimByDaysPrerequisites() {
        StringBuilder sb = new StringBuilder()
            .append("\n            SELECT id, operation,")
            .append("\n            table_name, column_name, row_id,")
            .append("\n            author, timestamp, comments,")
            .append("\n            data_type, old_val, new_val")
            .append("\n            FROM playsql_audit_trail")
            .append("\n            WHERE timestamp < NOW() - ? DAY")
            .append("\n            AND (operation = 'COL_DROP' OR operation = 'TABLE_DROP')")
            .append("\n            ORDER BY timestamp DESC -- We want table drops before column drops");
        return sb.toString();
    }


    @Override
    public String auditTrailTrimByDays() {
        StringBuilder sb = new StringBuilder()
            .append("\n            DELETE FROM playsql_audit_trail")
            .append("\n            WHERE timestamp < NOW() - ? DAY")
            .append("\n            AND operation <> 'ACTIVATION'");
        return sb.toString();
    }


}
