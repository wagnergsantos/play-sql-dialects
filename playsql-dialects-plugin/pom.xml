<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.playsql</groupId>
        <artifactId>playsql-dialect-parent</artifactId>
        <version>1.4.0-SNAPSHOT</version>
    </parent>
    <artifactId>playsql-dialects-plugin</artifactId>

    <name>Play SQL Dialects - Plugin</name>
    <!-- This 'jar' is both included in playsql-base-plugin and published for installation -->
    <description>Overrides the default dialects of Play SQL</description>
    
    <packaging>atlassian-plugin</packaging>

    <dependencies>
        <dependency>
            <groupId>com.playsql</groupId>
            <artifactId>playsql-dialect-api</artifactId>
            <version>1.4.0-SNAPSHOT</version>
            <type>jar</type>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>com.playsql</groupId>
            <artifactId>playsql-spi</artifactId>
            <version>1.4.0-SNAPSHOT</version>
            <type>jar</type>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.hsqldb</groupId>
            <artifactId>hsqldb</artifactId>
            <version>2.2.9</version>
            <!-- Yes, we ship it. See license in related files. -->
            <scope>compile</scope>
        </dependency>
        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>${postgresql.version}</version>
            <!-- Yes, we ship it. See license in related files. -->
            <scope>compile</scope>
            <exclusions>
                <exclusion>
                    <!-- It's a runtime dependency which isn't in postgresql.jar
                         Everything works when we add postgresql.jar in /lib, so why would it here? -->
                    <groupId>com.github.dblock.waffle</groupId>
                    <artifactId>waffle-jna</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <version>${slf4j.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
            <version>1.8.4</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>10.0.1</version>
            <!-- This lib is excluded when playsql-dialect.jar is embedded in playsql-base-plugin,
                 because it is provided by Confluence -->
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.velocity</groupId>
            <artifactId>velocity</artifactId>
            <version>1.6.2</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <!-- Launch Confluence: mvn amps:debug
                     CLI for the plugin: mvn amps:cli -Dproduct=confluence -Dcli.port=4337
                 -->
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>maven-amps-plugin</artifactId>
                <version>5.0.13</version>
                <extensions>true</extensions>
                <configuration>
                    <httpPort>1990</httpPort>
                    <contextPath>/confluence</contextPath>
                    <systemPropertyVariables>
                        <upm.pac.disable>true</upm.pac.disable>
                    </systemPropertyVariables>
                    <enableFastdev>false</enableFastdev>
                    <instanceId>confluence</instanceId>
                    <products>
                        <product>
                            <id>confluence</id>
                            <instanceId>confluence</instanceId>
                            <version>${confluence.version}</version>
                            <dataVersion>${confluence.version}</dataVersion>
                        </product>
                    </products>
                    <instructions>
                        <Import-Package>
                              com.playsql.dialect.model*;version="2.10",
                              com.playsql.dialect.exceptions;version="2.10",
                              com.playsql.dialect.api;version="2.10",
                              com.playsql.spi;version="2.10",
                              com.playsql.spi.models;version="2.10",
                              com.playsql.spi.utils;version="2.10",
                            org.springframework.jdbc.core;version="2.5",
                            com.google.common.base;resolution:=optional,
                            com.google.common.collect;resolution:=optional,
                            org.apache.commons.io;version="1.4",
                            org.apache.commons.lang;version="2.4",
                            org.apache.commons.lang3;version="3",
                            org.joda.time;version="1.6",
                            com.google.gson;version="1.6",
                            javax.xml.bind,
                            javax.xml.bind.annotation,
                            javax.annotation,
                            javax.ws.rs;version="1.0",
                            org.slf4j;version="1.5"
                        </Import-Package>
                    </instructions>

                    <pluginDependencies>
                        <pluginDependency>
                            <groupId>com.playsql</groupId>
                            <artifactId>playsql-base-plugin</artifactId>
                        </pluginDependency>
                    </pluginDependencies>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>build-helper-maven-plugin</artifactId>
                <version>1.8</version>
                <executions>
                    <execution>
                        <id>attach-bundle-artifact</id>
                        <phase>package</phase>
                        <goals>
                            <goal>attach-artifact</goal>
                        </goals>
                        <configuration>
                            <artifacts>
                                <artifact>
                                    <file>${project.build.directory}/${project.build.finalName}.jar</file>
                                    <type>jar</type>
                                </artifact>
                            </artifacts>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <!-- All the configuration is in the parent pom - We're just adding overrides -->
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>license-maven-plugin</artifactId>
                <configuration>
                    <excludes>
                        <!-- Those are in UTF-16 to match the getResourceAsStream(UTF-16) -->
                        <exclude>**/initial-data-*.sql</exclude>
                    </excludes>
                </configuration>
            </plugin>


            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>1.2.1</version>
                <executions>
                    <execution>
                        <id>process-reef-files</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>java</goal>
                        </goals>
                        <configuration>
                            <mainClass>com.playsql.dialect.reef.ReefTemplateEngineExecutor</mainClass>
                            <arguments></arguments>
                            <classpathScope>test</classpathScope>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
                <includes>
                    <include>**/*.properties</include>
                    <include>**/*.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>false</filtering>
                <excludes>
                    <exclude>**/*.properties</exclude>
                    <exclude>**/*.xml</exclude>
                </excludes>
            </resource>
            <resource>
                <!-- Make sure the license info lands into the final jar -->
                <directory>src/license</directory>
                <targetPath>META-INF/license</targetPath>
            </resource>
        </resources>
    </build>
    <properties>
        <confluence.version>5.9.12</confluence.version>
    </properties>
</project>
