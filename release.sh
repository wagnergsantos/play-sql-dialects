#!/bin/bash

# This script circumvents the absence of a distribution repository for Play SQL. It stores releases
# in ../releases
echo
echo "Usage ./release.sh 1.1.1 1.1.2-SNAPSHOT"
echo "The directory ../releases is required"
echo

set -e -u
VER="$1"
NEXT_VER="$2"
CURRENT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
CURRENT_COMMIT=$(git rev-parse HEAD)
LAST_COMMIT=$(git log -1 --pretty=oneline)
HAS_MVN=$(echo $LAST_COMMIT | grep "[auto] Set version to" || true)

echo
echo
echo "Going to release $VER and reset to $NEXT_VER"
echo "Current branch is $CURRENT_BRANCH and current commit $CURRENT_COMMIT"
echo "Has mvn: $HAS_MVN"
echo

if [ -n "$HAS_MVN" ] ; then
	git log HEAD^^^..HEAD --format=oneline
	echo
	echo "Will reset to previous commit (and remove the tag playsql-dialect-parent-$VER if exists)"
	read -p "Press ENTER"
	git reset --hard HEAD^

	LAST_COMMIT=$(git log -1 --pretty=oneline)
	HAS_MVN=$(echo $LAST_COMMIT | grep "maven-release-plugin" || true)
	if [ -n "$HAS_MVN" ] ; then
		git reset --hard HEAD^
	fi

	git tag -d playsql-dialect-parent-$VER || true

	echo "Last rev: "
	git log -1 --format=oneline
	echo
	git push --force
	echo 
fi

echo "Press any key to continue"
read

echo
echo
echo "Changing version to $VER"
echo
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$VER
mvn clean install
git commit -am "[auto] Set version to $VER"
git tag -a playsql-dialect-parent-$VER -m "Release $VER"

# We don't move the other artifacts to ../releases, since the local maven repo is enough to publish them
ls playsql-dialects-plugin/target/*.obr
cp playsql-dialects-plugin/target/*.obr ../releases/

echo
echo
echo "Changing version to $NEXT_VER"
echo
mvn versions:set -DgenerateBackupPoms=false -DnewVersion=$NEXT_VER
mvn clean install
git commit -am "[auto] Set version to $NEXT_VER"

echo
echo
echo "Pushing..."
echo
git push --tags

echo
echo
echo "Get cracking, now"
echo
